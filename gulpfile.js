const 
	gulp = require("gulp"),
	rjs = require("requirejs"),
	baseConfig = require("./bundle.config.js"),
	ModulesAnalyzer = require("./modulesAnalyser.js");


const BASE_DIR = "/public/js/app/user";
const MODULES_DIR = BASE_DIR + "/modules";
const CORE_CONFIG_FILE_NAME = BASE_DIR + "/core/config.js";
const CORE_MODULE_NAME = "core/core";

gulp.task("default", function() {
	
	const analyzer = new ModulesAnalyzer();

	const moduleNames = analyzer.readModuleNames(CORE_CONFIG_FILE_NAME, BASE_DIR);
	const allModules = analyzer.findDistinctModuleNames(moduleNames.controllerFullPaths, []);
	
	baseConfig.shim[CORE_MODULE_NAME] = {
		deps: allModules
	};


	return rjs.optimize(baseConfig);
});
