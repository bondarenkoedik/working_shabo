Crud = require '../../lib/crud'

crud = new Crud
	modelName: 'Offer'

module.exports.rest = crud.request.bind crud
