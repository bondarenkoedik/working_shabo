Crud = require '../../lib/crud'

crud = new Crud
  modelName: 'DegustationType'

module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud
