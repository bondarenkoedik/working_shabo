Crud = require '../../lib/crud'
View = require '../../lib/view'
Model = require '../../lib/model'
Logger = require '../../lib/logger'
async = require 'async'

crud = new Crud
	modelName: 'ProductCategory'
	files: [
		name: 'mainHorizontal'
		replace: true
		type: 'string'
		parent: 'img'
	,
		name: 'mainVertical'
		replace: true
		type: 'string'
		parent: 'img'
	,
		name: 'mainTabletHorizontal'
		replace: true
		type: 'string'
		parent: 'img'
	,
		name: 'mainTabletVertical'
		replace: true
		type: 'string'
		parent: 'img'
	,
		name: 'bg'
		replace: true
		type: 'string'
		sizes: [500]
		parent: 'img'
	,
		name: 'item'
		replace: true
		type: 'string'
		parent: 'img'
	]

module.exports.findWithProducts = (req, res) ->
	data = req.params
	result = {}

	async.waterfall [
		(next) ->
			Model 'ProductCategory', 'findOne', next, {link: data.link}, null, {lean: true}
		(doc, next) ->
			if doc
				result = doc
				Model 'Product', 'find', next, {productCategory: doc._id}, null, {sort: {'position': 1}}
			else
				next 404, "No category exists with link: #{data.link}"
		(docs, next) ->
			if docs
				result.products = docs

			View.clientSuccess data: result, res
	], (err) ->
		msg = "Error in #{@options?.filename}: #{err.message or err}"
		Logger.log 'error', msg
		View.clientFail err, res

module.exports.rest = crud.request.bind crud
module.exports.restFile = crud.fileRequest.bind crud
