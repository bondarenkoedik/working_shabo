async = require 'async'
_ = require 'lodash'
nodemailer = require 'nodemailer'
smtpTransport = require 'nodemailer-smtp-transport'

View = require '../../lib/view'
Model = require '../../lib/model'
Logger = require '../../lib/logger'

config = require '../../meta/config'



exports.get = (req, res) ->
	return res 400 unless req.query or req.query.modelField or req.query._id

	modelField = req.query.modelField
	_id = req.query._id

	async.waterfall [
		(next) ->
			query = {}
			query[modelField] = _id

			commentaries = Model 'Commentary', 'find', null, query
			commentaries.populate("user #{modelField}").exec next
		(docs, next) ->
			View.clientSuccess docs, res
	], (err) ->
		View.clientError err, res



exports.add = (req, res) ->
	userId = req.user?._id
	modelId = req.body?._id
	modelField = req.body?.modelField
	content = req.body?.form?.content

	return res.send "Error in controllers/user/article/index: Not all of the variables were received" unless content or modelField or modelId or userId

	async.waterfall [
		(next) ->
			data = {
				user: userId,
				content: content
			}
			data[modelField] = modelId

			Model 'Commentary', 'create', next, data
		(doc, next) ->
			View.clientSuccess doc, res

	], (err) ->
		View.clientError err, res



exports.askExpert = (req, res) ->
	userId = req.user?._id
	content = req.query?.content

	return res.send "Error in controllers/user/commentary/askExpert: Not all of the variables were received" unless content or userId

	async.waterfall [
		(next) ->
			sendExpertQuestion content, req.user, next
		(info, next) ->
			View.clientSuccess info, res

	], (err) ->
		View.clientError err, res



sendExpertQuestion = (content, user, cb) ->

	transporter = nodemailer.createTransport(smtpTransport(
		host: config.mail.host
		port: config.mail.port
		auth:
			user: config.mail.user
			pass: config.mail.pass))

	mailOptions =
		from: config.registrationEmail
		to: config.expertEmail
		subject: "Вопрос эксперту"
		text: user?.email + ": " + content

	transporter.sendMail mailOptions, (error, info) ->
		if error
			cb null, error
		else
			if info?.error
				cb null, info.error
			else
				cb null, info