express = require 'express'
passport = require 'passport'

View = require '../lib/view'

Main = require './user/main'

ProductCategory = require './admin/productCategory.coffee'
Product = require './admin/product.coffee'
ArticleCategory = require './admin/articleCategory.coffee'
Article = require './admin/article.coffee'
Award = require './admin/award.coffee'
GalleryImage = require './admin/galleryImage.coffee'
Vacancy = require './admin/vacancy.coffee'
VacancyResponse = require './admin/vacancyResponse.coffee'
GalleryVideo = require './admin/galleryVideo.coffee'
Contact = require './admin/contact.coffee'
Feedback = require './admin/feedback.coffee'
Faq = require './admin/faq'
Auth = require '../lib/auth'
Author = require './admin/author'
Degustation = require './admin/degustation'
DegustationType = require './admin/degustationType'
Commentary = require './user/commentary'
Offer = require './admin/offer'
Food = require './admin/food'
Country = require './admin/country'

Router = express.Router()

socialConfig = require '../meta/socialConfig'

Router.use (req, res, next) ->
	ie = (/MSIE ([0-9]{1,}[\.0-9]{0,})/g).exec req.headers['user-agent']
	if ie is null
		next()
	else
		version = parseFloat ie[0].replace('MSIE ', '')
		if version > 8
			next()
		else
			Main.ie req, res


Router.get '/', Main.index

#------- Page ---------#

Router.get '/page/:link', Main.getPage

#------- ProductCategories ---------#

Router.get '/productCategory', ProductCategory.rest
Router.get '/productCategory/:link', ProductCategory.findWithProducts
Router.get '/productCategories/:link', ProductCategory.findWithProducts

#------- Products ---------#

Router.get '/product', Product.rest

#------- ArticleCategories ---------#

Router.get '/articleCategory', ArticleCategory.rest

#------- Articles ---------#

Router.get '/article', Article.rest
Router.get '/article/:link', Article.rest
Router.get '/articles/:link', Article.rest

#------- Awards ---------#

Router.get '/award', Award.rest

#------- GalleryImages ---------#

Router.get '/galleryImage', GalleryImage.rest

#------- Vacancy ---------#

Router.get '/vacancy', Vacancy.rest
Router.post '/vacancyResponse', VacancyResponse.rest

#------- Gallery Videos ---------#

Router.get '/galleryVideo', GalleryVideo.rest

#------- Contacts ---------#

Router.get '/contact', Contact.rest

#------- Feedback ---------#

Router.use '/feedback/:id?', Feedback.rest

#------- Faqs ---------#

Router.get '/faq', Faq.rest

#------- Author ---------#

Router.get '/author/:id?', Author.rest

#------- Offers ---------#

Router.get '/offer', Offer.rest

#------- Food ---------#

Router.get '/food', Food.rest

#------- Degustation ---------#

Router.get '/degustation', Degustation.rest

#------- Degustation Type ---------#

Router.get '/degustationType', DegustationType.rest

#------- Country ---------#

Router.get '/country', Country.rest

#------- Registration ---------#

Router.post '/user/register', Auth.register
Router.post '/user/login', Auth.userLogin
Router.post '/user/logout', Auth.userLogout
Router.post '/user/getDiscount', Auth.getDiscount
Router.post '/user/facebook', Auth.facebook
Router.post '/user/vk', Auth.vk
Router.post '/user/recoverPassword', Auth.recoverPassword
Router.post '/user/resetPassword', Auth.resetPassword
Router.post '/user/sendFullRegistrationEmail', Auth.sendFullRegistrationEmail

#------- Commentaries ---------#

Router.get '/commentaries/get', Commentary.get
Router.post '/commentaries/add', Commentary.add
Router.post '/commentaries/askExpert', Commentary.askExpert

exports.Router = Router
exports.layoutPage = Main.index
