const fs = require("fs");

class ModulesAnalyzer {
	findDistinctModuleNames(modules, justAdd) {
		let results = new Set();
		for (var i = 0; i < modules.length; i++) {
			const controller = modules[i];
			const controllerDeps = this._extractDependencies(controller);
			controllerDeps.forEach(dependency => results.add(dependency));
		}

		justAdd.forEach(i => results.add(i));

		return Array.from(results);
	}

	readModuleNames(configName, baseDir) {
		let modules;

		let tmp = global.define;
		global.define = function(config) {
			modules = config.router.modules.map(o => o.path);
		};

		const content = fs.readFileSync(__dirname + configName, "utf8");
		eval(content);

		global.define = tmp;

		const EXT = ".js";
		const jsModules = this._normalizeModuleNames(modules);
		const viewModules = this._viewsLocator(jsModules, baseDir).reduce((a, b) => a.concat(b));

		// const allModules = jsModules.map(module => baseDir + "/" + module + EXT).concat(viewModules);

		// console.log(allModules);

		return {
			views: viewModules,
			controllerFullPaths: jsModules.map(module => baseDir + "/" + module + EXT),
			controllers: modules
		};
	}

	_extractDependencies(fileName) {
		let dependencies;

		let tmp = global.define;
		global.define = function(deps) {
			dependencies = deps;
		};
		let content = fs.readFileSync(__dirname + fileName, "utf8");
		eval(content);
		global.define = tmp;

		return dependencies;
	}

	_normalizeModuleNames(modules) {
		const PREFIX = "app/";
		return modules.map(module => module.slice(PREFIX.length));
	}

	_viewsLocator(modules, baseDir) {
		const VIEW_EXT = ".stache";
		const VIEWS_LOCATION_DIR = "/views/";
		return modules.map(module => {
			let endsNameIndex = module.lastIndexOf("/");
			const moduleDirName = module.slice(0, endsNameIndex) + VIEWS_LOCATION_DIR;
			const files = fs.readdirSync(__dirname  + baseDir  + "/"+ moduleDirName);

			const VIEW_PREFIX = "css!app/";

			return files.filter(viewFullPath => ~viewFullPath.indexOf(VIEW_EXT))
				.map(viewName => {
					const viewModuleName = moduleDirName + viewName;
					return VIEW_PREFIX + viewModuleName;
				});
		});
	}
}

module.exports = ModulesAnalyzer;