define([
	'canjs',
	'lib/viewport'
],
	function (can, viewport) {		
		var AppState = can.Map.extend({
				//Settings
				imgPath: '/img/',
				uploadPath: '/uploads/',

				locale: window.data.locale,
				lang: window.data.lang,
				socialConfig: window.data.socialConfig,
				user: window.data.user,

				toggleFullMenu: 'Open',
				blurred: false,

				productSlide: 0,
				registrationDisabled: 'disabled',
				userActivation: '',
				rulesPopupDisabled: 'disabled',
				mapData: null
			}),
			appState = new AppState();

		delete window.data;
		$('.scriptData').remove();

		return appState;
	}
);
