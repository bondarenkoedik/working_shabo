require.config({
	baseUrl: '/js/lib',
	urlArgs: 'v=0.1.69',
	paths: {
		cssDir: '../../css/user',
		app: '../app/user',
		lib: '../app/admin/lib',
		userLib: '../app/user/lib',
		src: '../app/user/src',
		can: 'canjs/amd/can/',
		canjs: 'canjs/amd/can',
		core: '../app/user/core',
		jquery: 'jquery/dist/jquery.min',
		underscore: 'underscore/underscore',
		modules: '../app/admin/modules',
		components: '../app/admin/components',
		viewHelpers: '../app/admin/core/viewHelpers',
		cssComponents: '../../css/admin/components',
		velocity: 'velocity/velocity.min',
		'custom-scrollbar': 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',
		'jquery-ui': 'jquery-ui-custom/jquery-ui.min',
		'skrollr': 'skrollr/skrollr',
		'parallax': 'parallax/parallax'

		// Social
		//fb: '../app/user/src/networks/fb-sdk',
		//vk: '../app/user/src/networks/vk-sdk',
		//ok: '../app/user/src/networks/ok-sdk'
	},
	map: {
		'*': {
			'css': 'require-css/css'
		}
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'underscore': {
			exports: '_'
		},
		'funcunit': {
			exports: 'F'
		},
		'canjs': {
			deps: [
				'jquery',
				'can/route/pushstate',
				'can/map/define',
				'can/map/delegate',
				'can/map/sort',
				'can/list/promise',
				'can/construct/super'
			]
		},
		velocity: {
			deps: [
				'jquery'
			]
		},
		'fb': {
			exports: 'FB'
		},
		'vk': {
			exports: 'VK'
		},
		'ok': {
			exports: 'OK'
		},
		'custom-scrollbar': {
			deps: [
				'jquery'
			]
		},
		'parallax': {
			deps: [
				'velocity'
			]
		}

	},
	waitSeconds: 0
});


require([
		'app/router/router',
		'app/modules/header/header',
		'app/modules/global/global',
		'core/config',
		'core/appState',
		'core/viewHelpers',
		'viewHelpers',
		'velocity',

		'src/sprintf',

		'css!cssDir/reset.css',
		'css!cssDir/base.css'
	],
	function (
		Router,
		Header,
		Global,
		config,
		appState
	) {

		var body = $('body');

		var windowWidth = $(window).width();

		if (windowWidth > 1024) {
			appState.attr('deviceViewport', '');
		} else if (windowWidth >= 768) {
			appState.attr('deviceViewport', '_tablet');
		} else {
			appState.attr('deviceViewport', '_mobile');
		}

		appState.attr('router', new Router(body, config.router));
		appState.attr('header', new Header(body));
		appState.attr('global', new Global(body));
	}
);
