'use strict';

(function(){

	var component = 'awards';

	define(
		[
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'underscore',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, bxSlider) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						options = self.options;
					self.components = null;
					self.year = can.route.attr('year');

					ComponentModel.findAll({queryOptions: {
						'aggregate': 'year',
						'sort': 'position'
					}}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.components = data;

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
						self.initYearsWidth();
						var index = data.length - 1;

						if (self.year) {
							for (var i = 0, length = data.length; i < length; i++) {
								if (data[i]._id === self.year) {
									index = i;
									break;
								}
							}
						}

						$('.year').eq(index).trigger('click');
					}
				},

				initYearsWidth: function () {
					var $years = $('.year');

					$('.yearsContent').css('width', function () {
						return 90 * $years.length
					});
					$('.years').scrollLeft(10000);
				},

				'.year click': function (el, ev) {
					var self = this;

					if (!el.hasClass('active')) {

						$('.year').removeClass('active');
						el.addClass('active');

						var $awardsList = $('.awardsList', self.element);
						var awards = self.components[el.data('index')];

						can.route.attr('year', awards._id);

						$awardsList.velocity('fadeOut', function () {

							$awardsList.html(can.view(self.options.viewpath + 'awards.stache', {
									awards: awards,
									appState: appState
								}, {

								})
							);

							$awardsList.velocity('fadeIn');
						});
					}
				}
			});

		}
	);

})();