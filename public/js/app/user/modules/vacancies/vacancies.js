'use strict';

(function(){

	var component = 'vacancies';

	define([
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'modules/vacancyResponses/vacancyResponsesModel',
			'jquery-form/jquery.form',
			'velocity',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, VacancyResponsesModel) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this;

					var url = '/page/vacancies';
					if ( appState.attr('lang').length > 0) {
						url = '/'+appState.attr('lang')+'/page/vacancies'
					}
					can.ajax({
						url: url
					}).done(function (data) {

						if (data && data.data) {
							self.vacancies = data.data;
						}

						ComponentModel.findAll({}, function (data) {
							self.data = data;
							self.render(data);
						});
					}).fail(function () {

					});
				},

				render: function (data) {
					var self = this;

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: data,
							vacancies: self.vacancies,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				'.toggleCollapse click': function (el, ev) {
					var _id = el.data('href');
					var $content = $('#'+_id);

					if ($content.hasClass('visible')) {
						$content.velocity('slideUp');
						el.removeClass('visible');
						$content.removeClass('visible');
					} else {
						$content.velocity('slideDown');
						el.addClass('visible');
						$content.addClass('visible');
					}
				},

				'.respondToVacancy click': function (el, ev) {
					var $responseWrapper = $('.responseWrapper');

					$responseWrapper.find('input[name=vacancy]').val(el.data('vacancy_id'));
					$responseWrapper.velocity('fadeIn');
				},

				'.responseBackground click': function (el, ev) {
					this.hideResponse();
				},

				hideResponse: function () {
					$('.responseWrapper').velocity('fadeOut');
				},

				'input.validate keyup': 'textValidate',
				'textarea.validate keyup': 'textValidate',

				textValidate: function (el, ev) {
					var $form = el.parents('form');
					var $submit = $form.find('[type="submit"]');

					if (el.val().length > 0) {
						el.removeClass('wrong').addClass('correct');

						if ($form.find('.wrong').length == 0) {
							$submit.attr('disabled', false);
						}

					} else if ( el.val().length == 0 ) {
						el.removeClass('correct').addClass('wrong');
						$submit.attr('disabled', 'disabled');
					}
				},

				'#cvFile change': function (el, ev) {
					var self = this;

					var allowedExtensions = ['.doc', '.pdf'];
					var fileAllowed = _.find(allowedExtensions, function (extension) {
						if ( ev.target.files[0].name.indexOf(extension) > -1 ) return true
					});

					if (fileAllowed) {
						el.parents('.fileUpload').find('span').html(ev.target.files[0].name);
						el.removeClass('wrong').addClass('correct');
						el.parents('#sendCVForm').find('.submitButton').attr('disabled', false);
					} else {
						self.notification(appState.locale.vacancyFileUnallowed.title, appState.locale.vacancyFileUnallowed.content);
					}
				},

				'.responseForm submit': function (el, ev) {
					ev.preventDefault();

					var self = this,
						form = el.ajaxSubmit({
							method: 'POST',
							url: '/vacancyResponse'
						});

					form.data('jqxhr').done(function () {
						self.notification(appState.locale.vacancyResponseSuccess);
					}).fail(function () {
						self.notification(appState.locale.vacancyResponseFailure);
					});
				},

				'#sendCVForm submit': function (el, ev) {
					ev.preventDefault();
					var $activityMessageContainer = $('#sendCVForm').find('.fileUpload').find('span');
					$activityMessageContainer.html(appState.locale.loading);

					var self = this,
						form = el.ajaxSubmit({
							method: 'POST',
							url: '/vacancyResponse'
						});

					form.data('jqxhr').done(function () {
						$activityMessageContainer.html(appState.locale.fileSent);
						self.notification(appState.locale.vacancyResponseSuccess.title, appState.locale.vacancyResponseSuccess.content);
					}).fail(function () {
						self.notification(appState.locale.vacancyResponseFailure.title, appState.locale.vacancyResponseFailure.content);
					});
				},

				notification: function (title, content) {
					var $notificationWrap = $('.notificationWrapper');
					var $notification = $notificationWrap.find('.notification');

					var message = '<div class="closeBtn close"></div>' + '<h3>'+title+'</h3>' + '<div>'+content+'</div>';

					$notification.html(message);
					$notificationWrap.velocity('fadeIn');

					setTimeout(function(){
                        $notificationWrap.velocity('fadeOut');
					},3000);
				},

				'.notificationWrapper .close click': function (el, ev) {
					$('.notificationWrapper').velocity('fadeOut');
				},

				'.closeVacancyResponse click': function (el, ev) {
					ev.preventDefault();
					this.hideResponse();
				}

			});

		}
	);

})();