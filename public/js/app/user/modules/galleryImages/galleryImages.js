'use strict';

(function(){

	var component = 'galleryImages';

	define([
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',
			'css!app/modules/'+component+'/css/'+component+'.css',

			'css!light-gallery/css/lightGallery.css',
			'light-gallery/js/lightGallery'

/*			'css!gamma-gallery/css/style.css',

			'gamma-gallery/js/modernizr.custom.70736',
			'gamma-gallery/js/jquery.masonry.min',
			'gamma-gallery/js/js-url.min',
			'gamma-gallery/js/jquerypp.custom',
			'gamma-gallery/js/gamma'*/
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this;

					ComponentModel.findAll({}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
						self.initGallery();
					}
				},

				initGallery: function () {

					$("#gallery").lightGallery({
						html: true,
						loop: true,
						showThumbByDefault:true,
						addClass:'showThumbByDefault'
					});

				}
			});

		}
	);

})();