'use strict';

(function(){

	var component = 'author',
		componentPlural = 'authors';

	define(
		[
			'canjs',
				'modules/'+componentPlural+'/'+componentPlural+'Model',
			'core/appState',
			'modules/articles/articlesModel',
			'userLib/commentaries/commentaries',
				'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, ComponentModel, appState, ArticlesModel, Commentaries) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {

					var self = this,
						options = self.options,
						link = can.route.attr('id');

					this._id = null;
					self.articles = null;

					ComponentModel.findBy({link: link}, function (data) {
						self.data = data.data;
						self.render(data.data);

						self.initChangeOrientationEvent();
					});
				},

				render: function (data) {
					var self = this;

					if (data) {
						self._id = data._id;

						self.element.html(can.view(self.options.viewpath + (data.view ? data.view + appState.attr('deviceViewport') : 'index' + appState.attr('deviceViewport')) + '.stache', {
								component: data,
								appState: appState
							}, {

							})
						);

						ArticlesModel.findBy({
							author: data._id
						}, function (data) {
							self.renderArticles(data.data);
						});
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}
				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.render(self.data);
						},200);
					});
				},

				renderArticles: function (articles) {

					var self = this;

					if (!articles) return false;

					self.articles = articles;
					var paginatedData = self.articles.slice(0, 2);

					$('.articlesList', self.element).html(can.view(self.options.viewpath + 'articles.stache', {
							articles: paginatedData,
							appState: appState,
							displayMoreExpertArticles: self.articles.length > 2
						}, {

						})
					);
				},

				'.moreArticles click': function (el, ev) {
					var self = this;

					$('.articlesList', self.element).html(can.view(self.options.viewpath + 'articles.stache', {
							articles: self.articles,
							appState: appState,
							displayMoreArticles: false
						}, {

						})
					);

					el.hide();
				},

				'.closeAskExpert click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				},

				'.askExpertBtn click': function (el, ev) {
					if (appState.attr('user')) {
						$('.askExpertErrorWrap').css('display', 'none');
						$('.askExpertFormWrap').velocity('fadeIn');
						$('.askExpertWrapper').velocity('fadeIn');
					} else {
						$('.showRegistration').trigger('click');
					}
				},

				'.askExpertForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Commentaries.askExpert(el, can.proxy(self.askExpertCallback, self));
					$('.askExpertFormWrap').velocity('fadeOut');
				},

				askExpertCallback: function (data) {
					var self = this;
					var responseContent = appState.attr('locale.askExpertSuccess');
					var $errorWrap = $('.askExpertErrorWrap');

					if (data.errors && data.errors.length > 0) {
						responseContent = data.errors[0].response;
					}
					$errorWrap.find('.content').html(responseContent);
					$errorWrap.velocity('fadeIn');
				},

				'.askExpertErrorWrap .popupReady click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				}

			});

		}
	);

})();