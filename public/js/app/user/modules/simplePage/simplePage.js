'use strict';

(function (){

	var component = 'simplePage',
		componentRoute = 'page';

	define(
		[
			'canjs',
			'core/appState',
			'modules/offers/offersModel',
			'modules/countries/countriesModel',
			'modules/degustationTypes/degustationTypesModel',
			'modules/degustations/degustationsModel',
			'userLib/commentaries/commentaries',
			'skrollr',
			'userLib/auth/auth',
/*			'custom-scrollbar',*/
/*			'jquery-ui',*/
			// 'css!malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
			'css!app/modules/'+component+'/css/'+component+'.css'
		],

		function (can, appState, OffersModel, CountriesModel, DegustationTypesModel, DegustationsModel, Commentaries, skrollr, Auth) {

			return can.Control.extend({
				defaults: {
					viewpath: '/js/app/user/modules/'+component+'/views/'
				}
			}, {

				init: function () {
					var self = this,
						lang = appState.attr('lang'),
						link = can.route.attr('id');

					self.$window = $(window);
					self.direction = 1;
					self.animationBlock = true;
					self.scrollTop = self.$window.scrollTop();
					self.easing = [.25,.1,.25,1];
					self.toScroll = false;
					this.mouseDown = false;
					self.headerSlideAnimationCompleted = true;
					self.windowHeight = self.$window.height();
					self.$header = $('#header');
					self.parallaxItemsArray = [];
					self.parallaxItemsArrayReversed = [];
					self.svgObject = null;
					self.scrollTimeout = false;

					can.ajax({
						//url: '/'+componentRoute+'/' + link
						url: (lang ? '/' + lang : '') + '/'+componentRoute+'/' + link
					}).done(function (data) {
						self.data = data;
						self.render(data);
						self.initChangeOrientationEvent();
					}).fail(function () {
						self.loaded();
					});

				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.render(self.data);
						},200);
					});
				},

				'{window} resize': function() {
					var self = this;
					this.toScroll = true;

					//self.direction = 1;
					//self.animationBlock = true;
					self.scrollTop = self.$window.scrollTop();
					//self.headerSlideAnimationCompleted = true;
					self.windowHeight = self.$window.height();
					self.parallaxItemsArray = [];
					self.parallaxItemsArrayReversed = [];

					if (can.route.attr('id') == 'history' || can.route.attr('id') == 'production-process') {

						if (self.$window.width() > 1024) {

							var direction = false;

							if (self.windowHeight > self.$window.height()) {
								direction = 1;
							} else {
								direction = -1;
							}
							self.windowHeight = self.$window.height();

							self.$parallaxItems.css({height: self.$window.height() - self.$headerHeight});
							self.parallaxItemsHeight = self.$parallaxItems.height();

							if (can.route.attr('id') == 'history') {
								self.historyOnLoaded();
							} else {
								self.productionProcessOnLoaded();
							}

							this.initParallaxItemsArray();

							var onScrollTimeout;
							clearTimeout(onScrollTimeout);
							onScrollTimeout = setTimeout(function() {
								self.onScroll(direction);
							}, 100);

						}


					} else {
						//this.render(this.data);
					}
				},

				render: function (data) {
					var self = this;
					self.view = data.data.view;

					self.element.html(
						can.view(self.options.viewpath + (data.data.view ? data.data.view + appState.attr('deviceViewport') : 'index' + appState.attr('deviceViewport')) + '.stache', {
							appState: appState,
							component: data.data,
							lang: appState.attr('lang')
						})
					);
					self.loaded();
				},

				loaded: function () {
					var self = this;
					if (self.options.isReady) {
						self.options.isReady.resolve();

						self.$html = $("html");
						self.$footer = $('#footer');
						self.$verticalNavigation = $('.verticalNavigation', self.element);

						if (self.view == 'wine-map') {
							//self.initCustomScrollbar();
							appState.attr('deviceViewport')
						} else if (self.view == 'wineyards') {
/*							self.initPanoAnErrorEvent();*/
						} else if (self.view == 'shabo-club') {
							self.getOffers();
						} else if (self.view == 'history') {

							self.historyOnLoaded();

							if (appState.attr('deviceViewport') == '') {
								self.calculateHistorySlideHeight();
							}

						} else if ( self.view == 'production-process' ) {

							self.productionProcessOnLoaded();

							if (appState.attr('deviceViewport') == '') {
								self.calculateProductionProcessSlideHeight();
							}
						} else if (self.view == 'export-markets' ) {
							self.getMapData();
						} else if (self.view == 'degustation') {
							self.getDegustations();
						}
					}
				},

				historyOnLoaded: function () {
					var self = this;

					if ($('#sp-production-process').length > 0) {
						location.reload();
					}

					self.menuHeight = self.$verticalNavigation.height();
					self.$parallaxItems = $('.historyWrapper .historyItem');
					self.$historyNavigation = $('.historyNavigation a');
					self.$headerHeight = self.$header.outerHeight();
					self.submenuAnimation = false;

					self.$historyNavigationWrap = $('.historyNavigation');
				},

				productionProcessOnLoaded: function () {
					var self = this;

					if ($('#sp-history').length > 0) {
						location.reload();
					}

					self.menuHeight = self.$verticalNavigation.height();
					self.$parallaxItems = $('.productionProcessWrapper .productionProcessItem');
					self.$historyNavigation = $('.productionProcessNavigation a');
					self.$header = $('#header');
					self.$headerHeight = self.$header.outerHeight();
					self.submenuAnimation = false;

					self.$productionProcessNavigationWrap = $('.productionProcessNavigation');
				},

				initParallaxItemsArray: function () {
					var self = this;

					self.$parallaxItems.each(function() {

						self.parallaxItemsArray.push({
							element: $(this),
							offsetTop: $(this).offset().top
						});
						self.parallaxItemsArrayReversed.push({
							element: $(this),
							offsetTop: $(this).offset().top
						});
					});

					self.parallaxItemsArrayReversed.reverse();
				},

				getOffers: function () {
					var self = this;

					OffersModel.findAll({}, function (data) {
						self.renderOffers(data);
					});
				},

				renderOffers: function (data) {
					var self = this;

					$('.offers', self.element).html(can.view(self.options.viewpath + 'shabo-club-offers.stache', {
							components: data,
							appState: appState
						}, {

						})
					);
				},

				calculateHistorySlideHeight: function () {
					var self = this;

					$('.historyItem', self.element).css({height: self.$window.height() - self.$headerHeight});

					self.parallaxItemsHeight = self.$parallaxItems.height();
					self.initParallaxItemsArray();
					self.initParallax();
				},

				initParallax: function () {
					var self = this;

					var s = skrollr.init({
						smoothScrolling: true,
						smoothScrollingDuration: 2000,
						forceHeight: true,
						render: function(data) {
							if (self.view == 'history') {
								self.historyMenuStatus(data.curTop);
							} else if (self.view == 'production-process') {
								self.productionProcessMenuStatus(data.curTop);
							}
						}
					});

					self.initParallaxScrollEv();


					if (this.toScroll) {
/*
						var $html = $('html');
						$html.velocity('scroll', {
							offset: self.$window.scrollTop() + 100,
							duration: 300,
							easing: self.easing,
							completed: function () {
								self.toscroll = false;
							}
						});*/
					}
				},

				calculateProductionProcessSlideHeight: function () {
					var self = this;

					$('.productionProcessItem', self.element).css({height: self.$window.height() - self.$headerHeight});

					self.parallaxItemsHeight = self.$parallaxItems.height();
					self.initParallaxItemsArray();
					self.initParallax();
				},

				historyMenuStatus: function (top) {
					var self = this;
					var itemsHeight = self.parallaxItemsHeight;

					for (var i = 1, length = self.$parallaxItems.length; i <= length; i++) {
						if (top + 30 > itemsHeight * (i - 1) && top < itemsHeight * i) {
							self.$historyNavigation.removeClass('active');
							self.$historyNavigationWrap.find('a.slide-'+i+'-link').addClass('active');
						}
					}
				},

				'{window} mousedown': function (el, ev) {
					this.mouseDown = true;
				},

				'{window} mouseup': function (el, ev) {
					this.mouseDown = false;
				},

				':module route': 'routeChanged',
				':module/:id route': 'routeChanged',

				routeChanged: function(data) {
					var self = this;
					var hrefs, dataHrefs;

					if (data.id === 'history' || data.id === 'production-process') {

						if (data.id === 'history') {
							self.$parallaxItems = $('.historyWrapper .historyItem');
						} else {
							self.$parallaxItems = $('.productionProcessWrapper .productionProcessItem');
						}

						self.initParallaxScrollEv();
					} else {
						$(window).off('scroll.parallax');
					}
				},

                preventDefault: function(e) {
                    e.preventDefault();
				},

                disableScroll: function () {
                    $(document).on("keydown mousewheel DOMMouseScroll", this.preventDefault);
                    $(window).on("keydown mousewheel DOMMouseScroll", this.preventDefault);
                },

                enableScroll: function () {
                    $(document).unbind("keydown mousewheel DOMMouseScroll");
                    $(window).unbind("keydown mousewheel DOMMouseScroll");
                },

				initParallaxScrollEv: function () {
					var self = this;

					if (can.route.attr('id') == 'history' || can.route.attr('id') == 'production-process' && self.$window.width() > 1024) {

                        $(window).on('mousewheel', function (e) {
                            var direction = e.originalEvent.wheelDelta < 0 ? 1 : -1;
                            e.preventDefault();
                            if (!self.mouseDown && !self.submenuAnimation && self.headerSlideAnimationCompleted) {
                                self.onScroll(direction);
                            }
                        });

						// $(window).on('scroll', function (e) {
                        //
						// 	if (!self.mouseDown && !self.submenuAnimation && self.headerSlideAnimationCompleted) {
                         //        self.onScroll();
						// 	}
						// 	// self.alignVerticalNavigation(self.$window.scrollTop());
						// });
					}
				},

/*				'{window} scroll': function (el, event) {
					if (!this.mouseDown && !this.submenuAnimation ) {
						if (can.route.attr('id') == 'history' || can.route.attr('id') == 'production-process') {
							this.alignVerticalNavigation(this.$window.scrollTop());

							if ( this.headerSlideAnimationCompleted) {
								this.onScroll();
							}
						}
					}
				},*/

				onScroll: function (direction) {
					var scrollTop = this.$window.scrollTop();

					if (!direction) {
						if (scrollTop > this.scrollTop) {
							direction = 1;
						} else {
							direction = -1;
						}
					}

					this.scrollTop = scrollTop;

					if (this.animationBlock) {
						this.stickToSlide(direction, scrollTop);
					}

					this.toggleHeaderVisibility(scrollTop);
/*					this.alignVerticalNavigation(scrollTop);*/
				},

				stickToSlide: function (direction, scrollTop) {
                    this.animationBlock = false;
					var self = this;

                    if (direction == 1) {
						var res = _.find(self.parallaxItemsArray, function (item) {
							return item.offsetTop - self.$headerHeight > scrollTop;
						});

					} else {

						var res = _.find(self.parallaxItemsArrayReversed, function (item) {
							return item.offsetTop - self.$headerHeight < scrollTop;
						});
					}

					self.scrollAnimation(res);
				},

				scrollAnimation: function (res) {
					var self = this;

					if (res) {
						var targetElement = res.element;

						if ( targetElement.length > 0 ) {
							var targetOffset = targetElement.offset().top - self.$headerHeight;

							self.$html.velocity("scroll", {
								offset: targetOffset,
								duration: 2000,
								easing: self.easing,
								complete: can.proxy(self.scrollComplete, self)
							});

						} else {
							self.animationBlock = true;
						}
					} else {
						self.animationBlock = true;
					}

				},

				scrollComplete: function () {
					var self = this;

                    setTimeout(function () {
                        self.initParallaxItemsArray();
                        self.animationBlock = true;
                        self.alignVerticalNavigation(self.$window.scrollTop());
                    }, 100);

				},

				alignVerticalNavigation: function (scrollTop) {
					var self = this;
					var footerOffset = self.$footer.offset().top;
					var top = 145;

					if (scrollTop + self.menuHeight + top >= footerOffset){
						top = footerOffset - scrollTop - self.menuHeight;
					}

					self.$verticalNavigation.css({
						top: top
					});
				},

				toggleHeaderVisibility: function (scrollTop) {
					var self = this;
					var footerOffset = self.$footer.offset().top;

					if ( self.headerSlideAnimationCompleted == true ) {
						self.headerSlideAnimationCompleted = false;

						if (self.$header.css('display') != 'none' && scrollTop + self.$window.height() - self.$headerHeight >= footerOffset){
							self.$header.velocity("slideUp", {
								duration: 1500,
								complete: can.proxy(self.headerSlideComplete, self)
							});
						} else if (self.$header.css('display') == 'none' && scrollTop + self.$window.height() - self.$headerHeight < footerOffset) {
							self.$header.velocity("slideDown", {
								duration: 1500,
								complete: can.proxy(self.headerSlideComplete, self)
							});
						} else {
							self.headerSlideAnimationCompleted = true;
						}
					}
				},

				headerSlideComplete: function () {
					this.headerSlideAnimationCompleted = true;
				},

				productionProcessMenuStatus: function (top) {
					var self = this;
					var itemsHeight = self.parallaxItemsHeight;

					for (var i = 1, length = self.$parallaxItems.length; i <= length; i++) {
						if (top + 30 > itemsHeight * (i - 1) && top < itemsHeight * i) {
							self.$historyNavigation.removeClass('active');
							self.$productionProcessNavigationWrap.find('a.pp-slide-'+i+'-link').addClass('active');
						}
					}
				},

/*				initPanoAnErrorEvent: function () {
					var self = this;

					self.initDraggablePano();

					window.addEventListener("anError", function(e) {
						$('.wineyardPano').hide().remove();
						$('.wineyardPanoWrapper').show();

						self.initDraggablePano();
					});
				},*/

				initCustomScrollbar: function () {
					var self = this;

					$(".wineMapWrapper", self.element).mCustomScrollbar({
						theme: "rounded",
						axis: 'x',
						setLeft: '768px',
						scrollInertia: 400,
						scrollButtons: {
							enable: false,
							scrollAmount: 200,
							scrollType: 'stepless'
						}
					});
				},

/*				'.tileItem.privileges click': function (el, ev) {

					if (appState.attr('user')) {
						can.route.attr({
							module: 'sp',
							id: 'shabo-club'
						}, true);
					} else {
						$('.showRegistration').trigger('click');
					}
				},*/

				'.mapPlace click': function (el, ev) {
					var targetPlace = el.data('place');

					if ( el.hasClass('active') ) {
						el.removeClass('active');
						$('.mapPlaceDescription.active').removeClass('active');
					} else {
						$('.mapPlace.active').removeClass('active');
						el.addClass('active');
						$('.mapPlaceDescription.active').removeClass('active');

						$('.mapPlaceDescription.' + targetPlace).addClass('active');
					}
				},

				'.winePlace click': function (el, ev) {
					var targetPlace = el.data('place');

					if ( el.hasClass('active') ) {
						el.removeClass('active');
						$('.wineContent.active').removeClass('active');
					} else {
						$('.winePlace.active').removeClass('active');
						el.addClass('active');
						$('.wineContent.active').removeClass('active');

						$('.wineContent.' + targetPlace).addClass('active');
					}
				},

				'.standardList li mouseover': function (el, ev) {
					var liIndex = el.data('index');

					el.parents('.rangeItem').find('.degustationImage[data-index="'+liIndex+'"]').addClass('visible');
				},

				'.standardList li mouseout': function (el, ev) {
					var liIndex = el.data('index');

					el.parents('.rangeItem').find('.degustationImage[data-index="'+liIndex+'"]').removeClass('visible');
				},

				'.vipList li mouseover': function (el, ev) {
					var liIndex = el.data('index');

					$('.vipPointers').find('[data-index="'+liIndex+'"]').addClass('visible');
				},

				'.vipList li mouseout': function (el, ev) {
					var liIndex = el.data('index');

					$('.vipPointers').find('[data-index="'+liIndex+'"]').removeClass('visible');
				},

				'.degustationRangeBtn click': function (el, ev) {
					$('html, body').animate({
						scrollTop: $("#degustationRange").offset().top
					}, 400);
				},

/*				initDraggablePano: function () {
					var $wineyardPano = $( "#wineyardPano" );

					$wineyardPano.draggable({ axis: "x",
						stop: function(event, ui) {
							if(ui.position.left>0)
							{
								$wineyardPano.velocity({"left": "0px"}, 300);
							}
							else if(ui.position.left<-3976)
							{
								$wineyardPano.velocity({"left": "-3976px"}, 300);
							}
						}
					});

				},*/

				'.askExpertBtn click': function (el, ev) {
					var self = this;
/*					if ( self.view == 'shabo-club' && !appState.attr('user')) {
						$('.showRegistration').trigger('click');
						return
					}*/
					$('.askExpertErrorWrap').css('display', 'none');
					$('.askExpertFormWrap').velocity('fadeIn');
					$('.askExpertWrapper').velocity('fadeIn');
				},

				'.askExpertForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Commentaries.askExpert(el, can.proxy(self.askExpertCallback, self));
					$('.askExpertFormWrap').velocity('fadeOut');
				},

				askExpertCallback: function (data) {
					var self = this;
					var responseContent = appState.attr('locale.askExpertSuccess');
					var $errorWrap = $('.askExpertErrorWrap');

					if (data.errors && data.errors.length > 0) {
						responseContent = data.errors[0].response;
					}
					$errorWrap.find('.content').html(responseContent);
					$errorWrap.velocity('fadeIn');
				},

				'.askExpertErrorWrap .popupReady click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				},

				'.closeAskExpert click': function (el, ev) {
					$('.askExpertWrapper').velocity('fadeOut');
				},

				'.verticalNavigation a click': function (el, ev) {
					ev.preventDefault();
					var self = this;
					self.submenuAnimation = true;

					if (self.$header.css('display') == 'none'){
						self.$header.velocity("slideDown", {
							duration: 1500,
							complete: can.proxy(self.headerSlideComplete, self)
						});
					}

					$('html, body').animate({
						scrollTop: $(el.attr('href')).offset().top - self.$headerHeight
					}, 1000, can.proxy(self.submenuAnimationComplete, self));
				},

				'.horizontalNavigation a click': function (el, ev) {
					ev.preventDefault();

					$('html, body').animate({
						scrollTop: $(el.attr('href')).offset().top
					}, 1000);
				},

				submenuAnimationComplete: function () {
					var self = this;
					setTimeout(function () {
						self.submenuAnimation = false;
					}, 300);
				},

				'.offerItem click': function (el, ev) {
					if (appState.attr('user')) {
						$('#offerId').val(el.data('id'));
						$('.getDiscountPopup').addClass('visible');
					} else {

						$('.registrationPopup').velocity('fadeIn');
						$('.registrationPopupContent').css({
							'display': 'block',
							'opacity': 1
						});
						$('.loginPopupContent').css('display', 'none');

						appState.attr('blurred', 'blurred');
					}
				},

				'.getDiscountForm submit': function (el, ev) {
					var self = this;

					ev.preventDefault();

					Auth.getDiscount(el, can.proxy(self.getDiscountCallback, self));
				},

				getDiscountCallback: function (data) {
					$('.getDiscountPopupContent').hide();
					$('.getDiscountSuccessPopupContent').show();
				},

				'.closeGetDiscount click': function (el, ev) {
					$('.getDiscountPopup').removeClass('visible');
					$('.getDiscountPopupContent').show();
					$('.getDiscountSuccessPopupContent').hide();
				},

				'.export-markets path mouseenter': function (el, ev) {
					var id = el.attr('id');

					$('.export-markets').find('.'+id).addClass('active');
					SVG.select('path.'+el.attr('id')).addClass('active');

					if (id) {

						var country = _.find( appState.attr('mapData'), function (element) {
							return element.mapId == id;
						});

						if (country) {
							$('#'+country.mapId).css('fill', country.colorOn);
						}
					}
				},

				'.export-markets g mouseenter': function (el, ev) {
					var id = el.attr('id');

					$('.export-markets').find('.'+el.attr('id')).addClass('active');
					SVG.select('path.'+el.attr('id')).addClass('active');

					if (id) {
						var country = _.find( appState.attr('mapData'), function (element) {
							return element.mapId == id;
						});

						if (country) {
							$('#'+country.mapId).find('path').css('fill', country.colorOn);
						}
					}
				},

				'.export-markets path mouseleave': function (el, ev) {
					var id = el.attr('id');

					$('.export-markets .active').removeClass('active');
					SVG.select('path.active').removeClass('active');

					var country = _.find( appState.attr('mapData'), function (element) {
						return element.mapId == id;
					});

					if (country) {
						$('#'+country.mapId).css('fill', country.color);
					}
				},

				'.export-markets g mouseleave': function (el) {
					var id = el.attr('id');

					$('.export-markets .active').removeClass('active');
					// SVG.select('path.active').removeClass('active');

					var country = _.find( appState.attr('mapData'), function (element) {
						return element.mapId == id;
					});

					if (country) {
						$('#'+country.mapId).find('path').css('fill', country.color);
					}
				},

				getMapData: function () {
					var self = this;
					CountriesModel.findAll({}, function (data) {
						appState.attr('mapData', data);
						self.fillMap(data);
					});
				},

				arcLinks: function (dwg,x1,y1,x2,y2,n, skip, k, className) {
					var cx = (x1+x2)/2;
					var cy = (y1+y2)/2;
					var dx = (x2-x1)/2;
					var dy = (y2-y1)/2;
					var i;
					for (i=0; i<n; i++) {
						if (i > 0) {continue;}
						if (i==(n-1)/2) {
							dwg.line(x1,y1,x2,y2).stroke({width:1, dasharray: 2}).fill('none').addClass(className);
						}
						else {
							var dd = Math.sqrt(dx*dx+dy*dy);
							var ex = cx + dy/dd * k * (i-(n-1)/2);
							var ey = cy - dx/dd * k * (i-(n-1)/2);
							dwg.path("M"+x1+" "+y1+"Q"+ex+" "+ey+" "+x2+" "+y2).stroke({width:1, dasharray: 2}).fill('none').addClass(className);
						}
					}
				},

				fillMap: function (data) {
					var self = this;

					var langIndex = 0;
					if (appState.attr('lang') == '') {
						langIndex = 0
					} else if (appState.attr('lang') == 'ua') {
						langIndex = 1
					} else if (appState.attr('lang') == 'en') {
						langIndex = 2
					}

					var windowWidth = window.innerWidth;
					var topLabelShift = 0;
					var leftLabelShift = 0;
					var coefficient = 1024/1024;
					if (windowWidth < 1024) {
						coefficient = 688/1024;
						topLabelShift = 0 - (29 - 29 * coefficient);
						leftLabelShift = 0 - (10 - 10 * coefficient);
					}
					var topShift = 29*coefficient;
					var leftShift = 10*coefficient;

					var $ways = $('#ways-wrapper');
					var draw = SVG('ways-wrapper').size(1024 / coefficient, 555 / coefficient);

					$('#mapObject').on('load', function () {
						var $svgDoc = $(this.contentDocument);

						self.svgObject = $svgDoc;

						data.forEach(function(country) {

							$svgDoc.find('#'+country.mapId).css('fill', country.color);
							$svgDoc.find('#'+country.mapId).find('path').css('fill', country.color);
						});
					});

					data.forEach(function(country) {

						$ways.append('<object data="/img/export-markets/export-markets-pointer.svg" type="image/svg+xml" class="map-pointer country-label '+country.mapId+'" style="top: '+(topLabelShift+country.pointerTop*coefficient)+'px; left: '+(leftLabelShift+country.pointerLeft*coefficient)+'px"></object>');
						$ways.append('<div class="map-label country-label '+country.mapId+'" style="top: '+(topLabelShift+country.pointerTop*coefficient)+'px; left: '+(leftLabelShift+country.pointerLeft*coefficient)+'px">'+country.lang[langIndex].title+'</div>');

						if (parseInt(country.pointerLeft)*coefficient+10 > 535*coefficient) {
							self.arcLinks(draw, parseInt(country.pointerLeft)*coefficient+leftShift, parseInt(country.pointerTop)*coefficient+topShift, 535*coefficient, 161*coefficient, 2, 1, 180, country.mapId + ' country-label');
						} else {
							self.arcLinks(draw, 535*coefficient, 161*coefficient, parseInt(country.pointerLeft)*coefficient+leftShift, parseInt(country.pointerTop)*coefficient+topShift, 2, 1, 180, country.mapId + ' country-label');
						}

						$('#'+country.mapId).css('fill', country.color);
						$('#'+country.mapId).find('path').css('fill', country.color);

						if (windowWidth < 1024) {
							$('.countriesNamesContent').append('<button class="countryName" data-id="'+country.mapId+'">'+country.lang[langIndex].title+'</button>');
						}
					});

					if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
						setTimeout(function () {
							$('#mapObject').trigger('load');
						},3000);
					}

					self.initCountriesNamesWidth();
				},

				initCountriesNamesWidth: function () {
					var $years = $('.countryName');

					$('.countriesNamesContent').css('width', function () {
						return 110 * $years.length
					});
					$('.countriesNames').scrollLeft(10000);
				},

				mobileCountrySelect: function (el, self) {
					if (!el.hasClass('countryActive')) {

						var activeId = $('.countryActive').data('id');
						var id = el.data('id');
						var name = el.data('name');

						SVG.select('path.active').removeClass('active');
						SVG.select('path.'+id).addClass('active');

						$('.countryName').removeClass('countryActive');
						el.addClass('countryActive');

						$('.export-markets').find('.active').removeClass('active');
						$('.export-markets').find('.'+id).addClass('active');
						$('.export-markets').find('.'+name).addClass('active');

						if (activeId) {
							var countryActive = _.find( appState.attr('mapData'), function (element) {
								return element.mapId == activeId;
							});
							if (countryActive) {
								$('#'+countryActive.mapId).css('fill', countryActive.color);
								$('#'+countryActive.mapId).find('path').css('fill', countryActive.color);

								if (self.svgObject) {
									self.svgObject.find('#'+countryActive.mapId).css('fill', countryActive.color);
									self.svgObject.find('#'+countryActive.mapId).find('path').css('fill', countryActive.color);
								}
							}
						}

						var country = _.find( appState.attr('mapData'), function (element) {
							return element.mapId == id;
						});

						if (country) {
							$('#'+country.mapId).css('fill', country.colorOn);
							$('#'+country.mapId).find('path').css('fill', country.colorOn);

							if (self.svgObject) {
								self.svgObject.find('#'+country.mapId).css('fill', country.colorOn);
								self.svgObject.find('#'+country.mapId).find('path').css('fill', country.colorOn);
							}
						}
					}
				},

				'.countryName click': function (el, ev) {
					this.mobileCountrySelect(el, this);
				},

				getDegustations: function () {
					var self = this;
					var degustations = null;

					var langIndex = 0;
					if (appState.attr('lang') == '') {
						langIndex = 0
					} else if (appState.attr('lang') == 'ua') {
						langIndex = 1
					} else if (appState.attr('lang') == 'en') {
						langIndex = 2
					}

					DegustationsModel.findAll({}, function (data) {
						if (!data) {return;}
						data = _.sortBy(data, function (item) {
							return item.position;
                        });
						degustations = _.groupBy(data, function (degustation) {
							return degustation.degustationType._id;
						});
						var degustationTypes = [];
						for (var type in degustations) {
							degustationTypes.push({
								name: degustations[type][0].degustationType.lang[langIndex].name,
								snack: degustations[type][0].degustationType.lang[langIndex].snack,
								price: degustations[type][0].degustationType.price,
								degustations: degustations[type]
							});
						}
						self.renderDegustations(degustationTypes)
					})
				},

				renderDegustations: function (degustationTypes) {
					var self = this;

					$('#degustationRange', self.element).html(can.view(self.options.viewpath + 'degustations.stache', {
							components: degustationTypes,
							appState: appState
						}, {

						})
					);
				}
			});

		}
	);
})();
