'use strict';

(function (){

	var component = 'header';

	define([
			'canjs',
			'core/appState',
			'userLib/auth/auth',
			'velocity',
			//'social/vk/vk_sdk',
			//'social/fb/fb_sdk',
			'css!malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css',
			'jquery-form/jquery.form',
			'custom-scrollbar'
		],
		function (can, appState, Auth) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this,
						lang = appState.attr('lang');

					can.ajax({
						url: '/page/rules'
						//url: (lang ? '/' + lang : '') + '/'+componentRoute+'/' + link
					}).done(function (data) {
						self.data = data.data;
						self.renderComponent(data.data);

						self.initChangeOrientationEvent();
					}).fail(function () {
						self.loaded();
					});

					if (can.route.attr('id') == 'history' || can.route.attr('id') == 'production-process') {
						self.routeChanged({id: 'history'});
					}
				},

				checkUserActivation: function () {
					var self = this;
					var userActivation = self.getParameterByName('userActivation');

					if (userActivation && userActivation.length > 0) {
						appState.attr('userActivation', userActivation);
						$('.registrationConfirmationPopupSuccess').css('display', 'block');
						$('.registrationPopupContent').css('display', 'none');
						$('.registrationPopup').velocity('fadeIn');
					}
				},

				checkUserResetPassword: function () {
					var self = this;
					var resetPassword = self.getParameterByName('resetPassword');

					if (resetPassword && resetPassword.length > 0) {

						appState.attr('resetPassword', resetPassword);
						$('.resetPasswordFormWrap').css('display', 'block');
						$('.registrationPopupContent').css('display', 'none');
						$('.registrationPopup').velocity('fadeIn');
					}
				},

				'.closeRegistrationConfirmationSuccess click': function (el, ev) {
					$('.registrationConfirmationPopupSuccess').css('display', 'none');
					$('.loginPopupContent').css('display', 'block');

/*					console.log(appState.attr('userActivation'));

					Auth.sendFullRegistrationEmail({userActivation: appState.attr('userActivation')});*/
				},

				showActiveLanguage: function () {

					$('.isoLang[href="\/'+appState.attr('lang')+'"]').addClass('active');
				},

				renderComponent: function (data) {
					var self = this;

					$('#header').remove();
					$('#footer').remove();
					$('#healthWarning').remove();

					self.element.prepend(
						can.view(self.options.viewpath + 'index' + appState.attr('deviceViewport') + '.stache', {
							appState: appState,
							data: data,
							language: appState.attr('lang')
						})
					);

					self.element.append(
						can.view(self.options.viewpath + 'footer.stache', {
							appState: appState
						})
					);

					self.checkUserLoggedIn();
					self.initAgeCheck();
					self.initRulesScrollbar();

					self.checkUserActivation();
					self.checkUserResetPassword();
					self.showActiveLanguage();

					if (can.route.attr('id') == 'history' || can.route.attr('id') == 'production-process') {
						$('#header').addClass('history');
					}

					if (self.options.isReady) {
						self.options.isReady.resolve();
					}

					if ( $('.fullMenuToggle').hasClass('Close') ) {
						var $fullMenu = $('.fullMenu');
						var $contentCover = $('.contentCover');

						$fullMenu.slideDown();
						$fullMenu.addClass('visible');
						appState.attr('blurred', true);
						$contentCover.removeClass('hidden');
					}
				},

				initChangeOrientationEvent: function () {
					var self = this;

					$(window).on("orientationchange",function(){
						setTimeout(function() {
							self.renderComponent(self.data);
						},200);
					});
				},

				checkUserLoggedIn: function () {
					if (appState.attr('user') && appState.attr('user')._id) {
						$('.showRegistrationPopup').css('display', 'none');
						$('.logoutButton').css('display', 'inline');
					}
				},

				initAgeCheck: function () {
					if (!localStorage.getItem('isAdult')) {
/*						appState.attr('blurred', true);*/
						appState.attr('ageVisible', true);
/*						appState.attr('menuBlurred', true);*/
					}
				},

				initRulesScrollbar: function () {
					var self = this;

					setTimeout(function () {
						$(".rulesPopupContent .description").mCustomScrollbar({
							theme: "rounded"
						});
					}, 3000);
				},

				'.rulesPopupContent .close click': function (el, ev) {
					this.toggleRulesPopup();
				},

				'.rulesPopupContent button click': function () {
					this.toggleRulesPopup();
				},

				'.ageCheckYes click': function (el, ev) {
					appState.attr('blurred', false);
					appState.attr('ageVisible', false);
					localStorage.setItem('isAdult', true);
					appState.attr('menuBlurred', false);
				},

				'.fullMenuToggle click': function (el, ev) {
					var self = this;

					self.fullMenuToggle(el, ev);
				},

				fullMenuToggle: function (el, ev) {
					var self = this;

					appState.attr('toggleFullMenu') == 'Open' ? appState.attr('toggleFullMenu', 'Close') : appState.attr('toggleFullMenu', 'Open');

					var $fullMenu = $('.fullMenu');
					var $contentCover = $('.contentCover');

					if ($fullMenu.hasClass('visible')) {
						$fullMenu.slideUp();
						$fullMenu.removeClass('visible');
						appState.attr('blurred', false);
						$contentCover.addClass('hidden');
					} else {
						$fullMenu.slideDown();
						$fullMenu.addClass('visible');
						appState.attr('blurred', true);
						$contentCover.removeClass('hidden');
					}
				},

				'.contentCover click': function (el, ev) {
					var $fullMenu = $('.fullMenu');
					var $contentCover = $('.contentCover');

					appState.attr('toggleFullMenu') == 'Open' ? appState.attr('toggleFullMenu', 'Close') : appState.attr('toggleFullMenu', 'Open');

					$fullMenu.slideUp();
					$fullMenu.removeClass('visible');
					appState.attr('blurred', false);
					$contentCover.addClass('hidden');
				},

				'.menuContent a click': function (el, ev) {
					var $active = $('.menuContent .active');
					$active.removeClass('active');
					el.addClass('active');
				},

				'.video_cont mouseover': function (el, ev) {
					$('#lightGallery-action').css({
						visibility: 'visible',
						opacity: '1'
					});
				},

				'.video_cont mouseout': function (el, ev) {
					$('#lightGallery-action').css({
						visibility: 'hidden',
						opacity: '0'
					});
				},

				'#lightGallery-action mouseover': function (el, ev) {
					el.css({
						visibility: 'visible',
						opacity: '1'
					});
				},

				'.lightGallery-slide .object mouseover': function (el, ev) {
					$('#lightGallery-action').css({
						visibility: 'visible',
						opacity: '1'
					});
				},

				'.lightGallery-slide .object mouseout': function (el, ev) {
					$('#lightGallery-action').css({
						visibility: 'hidden',
						opacity: '0'
					});
				},

				'.showRegistrationPopup click': function (el, ev) {
					ev.preventDefault();

					var registeredOnce = localStorage.getItem('registeredOnce');


					this.fullMenuToggle();
					$('.registrationPopup').velocity('fadeIn');

					if (registeredOnce && registeredOnce.length > 0) {
						$('.loginPopupContent').css('display', 'block');
						$('.registrationPopupContent').css('display', 'none');
					} else {
						$('.registrationPopupContent').css({
							'display': 'block',
							'opacity': 1
						});
						$('.loginPopupContent').css('display', 'none');
					}

					appState.attr('blurred', 'blurred');
				},

				'.commentaryRegistration click': function (el, ev) {
					$('.registrationPopup').velocity('fadeIn');
					$('.registrationPopupContent').css({
						'display': 'block',
						'opacity': 1
					});
					$('.loginPopupContent').css('display', 'none');

					appState.attr('blurred', 'blurred');
				},

				'.commentaryAuthorization click': function (el, ev) {
					$('.registrationPopupContent').css('display', 'none');
					$('.loginPopupContent').css('display', 'block');
					$('.registrationPopup').velocity('fadeIn');

					appState.attr('blurred', 'blurred');
				},

				'.showRegistration click': function (el, ev) {
					$('.registrationPopup').velocity('fadeIn');
					$('.registrationPopupContent').css({
						'display': 'block',
						'opacity': 1
					});
					$('.loginPopupContent').css('display', 'none');

					appState.attr('blurred', 'blurred');
				},

				'.authorizationButton click': function (el, ev) {
					$('.registrationPopupContent').velocity('fadeOut', function(){
						$('.loginPopupContent').velocity('fadeIn');
					});
				},

				'#agreedWithRules change': function (el, ev) {

					if (appState.attr('registrationDisabled') == 'disabled') {
						appState.attr('registrationDisabled', '');
					} else {
						appState.attr('registrationDisabled', 'disabled');
					}
				},

				'.rulesPopup click': function () {
					this.toggleRulesPopup();
				},

				toggleRulesPopup: function () {
					if (appState.attr('rulesPopupDisabled') == 'disabled') {
						appState.attr('rulesPopupDisabled', '');
					} else {
						appState.attr('rulesPopupDisabled', 'disabled');
					}
				},

				'.registrationForm submit': function (el, ev) {
					var self = this;
					ev.preventDefault();

					if (el.find('button.disabled').length == 0) {
						Auth.register(el, can.proxy(self.registerCallback, self));
					}
				},

				registerCallback: function (data) {

					console.log(data);
					if (data.err) {
						var $errorWrap = $('.registrationPopupError');
						$errorWrap.find('.content').html(data.err.message);
						$('.registrationPopupContent').velocity('fadeOut', function() {
							$errorWrap.velocity('fadeIn');
						});
					} else if (data.info && data.info.errors && data.info.errors.length > 0) {
						var $errorWrap = $('.registrationPopupError');
						$errorWrap.find('.content').html(data.info.errors[0].response);
						$('.registrationPopupContent').velocity('fadeOut', function() {
							$errorWrap.velocity('fadeIn');
						});
					} else {

						localStorage.setItem('registeredOnce', true);

						$('.registrationPopupContent').velocity('fadeOut', function () {
							$('.registrationPopupSuccess').velocity('fadeIn');
						});
					}
				},

				'.closeRegistrationError click': function (el, ev) {
					$('.registrationPopupError').velocity('fadeOut', function () {
						$('.registrationPopupContent').velocity('fadeIn');
					});

					appState.attr('blurred', false);
				},

				'.registrationSuccessBtn click': function (el, ev) {
					$('.registrationPopupSuccess').velocity('fadeOut', function () {
						$('.loginPopupContent').velocity('fadeIn');
					});
				},

				'.authorizationForm submit': function (el, ev) {
					var self = this;
					ev.preventDefault();

					Auth.login(el, can.proxy(self.loginCallback, self));
				},

				loginCallback: function (data) {
					$('.registrationPopupContent').velocity('fadeOut');

					if (data.error && data.error.length > 0) {
						var $errorWrap = $('.loginPopupError');
						$errorWrap.find('.content').html(data.error);
						$('.loginPopupContent').velocity('fadeOut', function() {
							$errorWrap.velocity('fadeIn');
						});
					} else {
						appState.attr('user', data);

						$('.showRegistrationPopup').css('display', 'none');
						$('.logoutButton').css('display', 'inline');

						$('.loginPopupContent').velocity('fadeOut', function () {
							$('.loginPopupSuccess').velocity('fadeIn');
						});
					}
				},

				'.logoutButton click': function (el, ev) {
					Auth.logout();
				},

				logoutResponse: function (data) {
					if (data) {

						$('.showRegistrationPopup').css('display', 'inline');
						$('.logoutButton').css('display', 'none');
					}
				},

				'.closeLoginSuccess click': function (el, ev) {
					$('.registrationPopup').velocity('fadeOut', function() {
						$('.loginPopupSuccess').css('display', 'none');
					});

					appState.attr('blurred', false);
				},

				'.closeLoginError click': function (el, ev) {
					$('.loginPopupError').velocity('fadeOut', function () {
						$('.loginPopupContent').velocity('fadeIn');
					});
				},

				'.facebookLoginBtn click': function (el, ev) {
					var self = this;

					if (!el.hasClass('disabled')) {
						Auth.facebookLogin(can.proxy(self.fbLoginResponse, self));
					}
				},

				fbLoginResponse: function (response) {
					var self = this;
					self.loginCallback(response);
				},

				'.vkLoginBtn click': function (el, ev) {
					var self = this;

					if (!el.hasClass('disabled')) {
						Auth.vkLogin(can.proxy(self.vkLoginResponse, self));
					}
				},

				vkLoginResponse: function (response) {
					var self = this;

					self.loginCallback(response);
				},

				'.closeRegistrationPopup click': function (el, ev) {

					$('.registrationPopup').velocity('fadeOut');
					appState.attr('blurred', false);
					appState.attr('rulesPopupDisabled', 'disabled');
					$('.resetPasswordSuccess').css('display', 'none');
					$('.resetPasswordFormWrap').css('display', 'none');
				},

				'input.validate keyup': function (el, ev) {
					this.textValidate(el);
				},

				textValidate: function (el) {
					var $form = $('.registrationForm');

					if (el.val().length > 0) {
						el.removeClass('wrong').addClass('correct');
					} else if ( el.val().length == 0 ) {
						el.removeClass('correct').addClass('wrong');
					}

					var $wrong = $form.find('.wrong');
					if ($wrong.length > 0) {
						$wrong.each(function(index, el) {
							if ($(this).val().length > 0) $(this).removeClass('wrong').addClass('correct');
						});

						$form.find('button').attr('disabled', 'disabled');
					} else {
						$form.find('button').removeAttr('disabled');
					}
				},

				getParameterByName: function (name) {
					name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
					var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
						results = regex.exec(location.search);
					return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
				},

				'.forgotPasswordLink click': function (el, ev) {
					$('.loginPopupContent').velocity('fadeOut', function () {
						$('.forgotPasswordWrapper').velocity('fadeIn');
					});
				},

				'.forgotPasswordForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					Auth.recoverPassword(el, can.proxy(self.forgotPasswordCallback, self));
				},

				forgotPasswordCallback: function (data) {

				},

				'.fullMenuContent .module click': function (el, ev) {
					this.fullMenuToggle();
				},

				'.resetPasswordForm submit': function (el, ev) {
					ev.preventDefault();
					var self = this;

					if (el.find('input[name=password]').val() === el.find('input[name=password2]').val()) {
						el.find('.error').css('opacity', '0');
						Auth.resetPassword(el, can.proxy(self.resetPasswordCallback, self));
					} else {
						el.find('.error').css('opacity', '1');
					}

				},

				resetPasswordCallback: function (data) {
					if (data && data.success) {
						$('.resetPasswordFormWrap').css('display', 'none');
						$('.resetPasswordSuccess').css('display', 'block');
					}
				},

				'.resetPasswordSuccess .close click': function (el, ev) {
					$('.registrationPopup').velocity('fadeOut', function(){
						$('.resetPasswordSuccess').css('display', 'none');
						$('.registrationPopupContent').css('display', 'block');
					});
				},

				':module route': 'routeChanged',
				':module/:id route': 'routeChanged',

				routeChanged: function(data) {
					var self = this;
					var hrefs, dataHrefs;

					var $header = $('#header');
					var currentActive = $header.find('.active');

					if (data.id === 'main-page') {
						if (data.module === 'sp') {
							hrefs = $header.find('[href="'+data.module + '/' + data.id +'"]');
							dataHrefs = $header.find('[data-href="'+data.module + '/' + data.id +'"]');
						} else {
							hrefs = $header.find('[href^="' + data.module + '"]');
							dataHrefs = $header.find('[data-href^="' + data.module + '"]');
						}
						$header.removeClass('history').show();
					} else if (data.id === 'history' || data.id === 'production-process') {
						$header.addClass('history');
					} else {
						$header.removeClass('history');
						hrefs = $header.find('[href="'+data.module + '/' + data.id +'"]');
						dataHrefs = $header.find('[data-href="'+data.module + '/' + data.id +'"]');
					}

					currentActive.removeClass('active');
					self.showActiveLanguage();

					if (hrefs && hrefs.length > 0 && dataHrefs && dataHrefs.length > 0) {
						hrefs.addClass('active');
						dataHrefs.addClass('active');
					}
				},

				'.languagesWrapper a click': function (el, ev) {

				}
			});

		}
	);
})();