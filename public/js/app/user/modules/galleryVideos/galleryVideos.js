'use strict';

(function(){

	var component = 'galleryVideos';

	define([
			'canjs',
			'modules/'+component+'/'+component+'Model',
			'core/appState',

			'css!light-gallery/css/lightGallery.css',
			'css!app/modules/'+component+'/css/'+component+'.css',

			'light-gallery/js/lightGallery'
		],

		function (can, ComponentModel, appState) {

			return can.Control.extend({
				defaults: {
					viewpath: 'app/modules/'+component+'/views/'
				}
			}, {
				init: function () {
					var self = this;

					ComponentModel.findAll({}, function (data) {
						self.data = data;
						self.render(data);
					});
				},

				render: function (data) {
					var self = this;

					self.components = data;

					self.element.html(can.view(self.options.viewpath + 'index.stache', {
							components: data,
							appState: appState
						}, {

						})
					);

					if (self.options.isReady) {
						self.options.isReady.resolve();
						self.initVideoGallery();
					}
				},

				initVideoGallery: function () {
					$("#video").lightGallery();
				}
			});

		}
	);

})();