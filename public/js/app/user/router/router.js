define([
	'canjs',
	'core/appState',
	'underscore',
	'core/hub'
],
	function (can, appState, _, hub) {

		var Modules = can.Map.extend({
			loaderShown: true,

			modules: [],

			silentInit: function (ev, module, id) {
				this.initModule(module, id, true);
			},

			initModule: function (moduleName, id, silent) {
				var self = this,
					module = _.find(self.moduleTypes, function (module) {
						return module.name === moduleName
					});

				if (!module) {
					throw new Error("There no such module '" + moduleName + "', please check your configuration file");
				}

				if (self.checkModule(id, silent)) {
					return;
				}

				if (!silent) {
					this.showPreloader();
				}

				require([module.path], function (Module) {
					if (Module) {
						self.addModule(id);
						var isReady = can.Deferred();
						new Module('#' + id, {
							isReady: isReady
						});
						if (!silent) {
							self.activateModule(id, isReady);
						}
					} else {
						if (module.path) {
							throw new Error('Please check constructor of ' + module.path + '.js');
						} else {
							throw new Error('Please check existing of module "' + module.name + '"');
						}
					}
				});

			},

			checkModule: function (id, silent) {
				var module = _.find(this.modules, function(module){
						return module.id === id;
					}),
					exist = !_.isEmpty(module);

				if (exist && !silent) {
					this.activateModule(id);
				}
				return exist;
			},

			addModule: function (id) {
				this.modules.push({
					id: id,
					active: false
				});
			},

			activateModule: function (id, isReady) {
				_.map(this.modules, function (module) {
					module.attr('active', module.id === id);
				});

				this.changeColorSchema(id);
				this.hidePreloader(isReady);
			},

			changeColorSchema: function (id) {
				var greyArray = [
					'sp-cultural-wine-center',
					'sp-culture-centre-description',
					'sp-tours',
					'sp-degustation',
					'videos-main-page',
					'gallery-main-page',
					'feedback-main-page',
					'faqs-main-page'
				];

				var match = _.find(greyArray, function (route) {
					return route == id;
				});

				if (match) {
					if (!$('body').hasClass('grey')) {
						$('body').addClass('grey');
						$('#healthWarning').find('img').attr('src', '/img/greyhealthWarning.jpg');
					}
				} else {
					$('body').removeClass('grey');
					$('#healthWarning').find('img').attr('src', '/img/healthWarning.jpg');
				}
			},

			showPreloader: function () {
				if (!this.attr('loaderShown')) {
					this.attr('loaderShown', true);
					$('#preloader').show();
				}
			},

			hidePreloader: function (isReady) {
				if (this.attr('loaderShown')) {
					isReady.then(function() {
						this.attr('loaderShown', false);
						$('#preloader').hide();
						$('.isoLang[href="\/'+appState.attr('lang')+'"]').addClass('active');
					}.bind(this));
				}
			}

		});

		return can.Control.extend({
			defaults: {
				viewpath: '../app/user/router/views/',
				langBtn: '.isoLang'
			}
		}, {
			init: function (el, options) {
				this.Modules = new Modules({
					moduleTypes: this.options.modules
				});

				// console.log('init', window.location.pathname);

				if (appState.attr('lang').length !== 0) {
					var langMatch = _.find(window.location.pathname.split('/'), function (element) {
						return element === appState.attr('lang');
					});

					if (!langMatch) {
						var pathname = window.location.pathname.replace('/en', '').replace('/ua', '');
						document.location.href = (appState.attr('lang') ? '/' + appState.attr('lang') : '') + pathname;
					}
				}
				else {
                    var pathname = window.location.pathname,
						langMatch = _.find(pathname.split('/'), function (element) {
                        return element === 'ua' || element === 'en';
                    });
                    if (typeof langMatch !== 'undefined') {
                        createCookie('language', langMatch);
                        document.location.href = pathname;
					}
				}

				var html = can.view(this.options.viewpath + 'route.stache', {
						modules: this.Modules.attr('modules'),
						appState: appState
					}),
					lang = appState.attr('lang'),
					self = this;

				$(options.modulesContainer).prepend(html);

				// console.log('options.routes: ', options.routes);

				_.each(options.routes, function (route) {
					can.route(route.route, route.defaults ? route.defaults : {});
				});

				can.on.call(hub, 'silentModule', can.proxy(this.Modules.silentInit, this.Modules));

				can.route.bindings.pushstate = can.route.bindings.pushstate || {}; // EDUARD
				can.route.bindings.pushstate.root = (lang ? '/' + lang : '') + '/';
				//can.route.bindings.pushstate.root = '/';
				can.route.ready();

			},

			'.module click': function (el, ev) {
				ev.preventDefault();

                var href = el.attr('href') ? el.attr('href') : el.attr('data-href');

                // console.log('href', href);

				try {
                    if ( href ) {

                        var routeObj = can.route.deparam(href);

                        if (_.isEmpty(routeObj)) {
                        	let data = href.split('/'); // ["", "products", "igristoe_reservuarnoye_vino_shabo"] for example
                        	routeObj = { module: data[1], id: data[2], route: ":module/:id" };
                        }

                        if (!_.isEmpty(routeObj)) {
                            can.route.attr(routeObj, true);
                        } else {
                            throw new  Error("!There now such routing rule for '" + href + "', please check your configuration file");
                        }

                    } else {
                        throw new  Error("href parameter is undefined");
                    }
				} catch (e) {
					console.error(e);
				}
			},

			':module route': 'routeChanged',
			':module/:id route': 'routeChanged',

			routeChanged: function(data) {
				var moduleName = data.module,
					id = moduleName + (data.id ? '-' + data.id : '');

				// console.log('moduleName', moduleName);

				this.Modules.initModule(moduleName, id);
			},

			'{langBtn} click': function (el, ev) {
				ev.preventDefault();

				if (el.hasClass('active')) {
					var $parent = el.parents('.languagesWrapper');

					if ($parent.hasClass('active')) {
						$parent.removeClass('active');
					} else {
						$parent.addClass('active');
					}

				} else {
					var lang = el.attr('href').replace(/\//, ''),
						currentLink = '/' + can.route.param(can.route.attr());

					var cookieLang = lang;
					if (lang.length === 0) {
						cookieLang = 'ru';
					}

					createCookie('language', cookieLang);

					// document.location.href = (lang ? '/' + lang : '') + currentLink;
					document.location.href = (lang ? '/' + lang : ''); // EDUARD
				}

			}

		});
	}
);
