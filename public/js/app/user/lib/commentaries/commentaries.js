'use strict';

(function (){

    var component = 'auth';

    define([
            'canjs',
            'core/appState',
            'velocity',
            'jquery-form/jquery.form'
        ],
        function (can, appState) {

            return can.Control.extend({
                defaults: {
                    viewpath: ''
                },

                get: function (modelField, _id, cb) {
                    can.ajax({
                        url: '/commentaries/get',
                        type: 'GET',
                        data: {
                            modelField: modelField,
                            _id: _id
                        },
                        success: function (data) {
                            cb(data);
                        }
                    });
                },

                add: function(modelField, _id, el, cb) {
                    var self = this;

                    can.ajax({
                        url: '/commentaries/add',
                        type: 'POST',
                        data: {
                            form: can.deparam(el.serialize()),
                            modelField: modelField,
                            _id: _id
                        },
                        success: function (data) {
                            cb(data);
                        }
                    });
                },

                askExpert: function(el, cb) {
                    var self = this,
                        form = el.ajaxSubmit({
                            method: 'POST',
                            url: '/commentaries/askExpert'
                        });

                    form.data('jqxhr').done(function (data) {
                        cb(data);
                    }).fail(function (data) {
                        cb(data);
                    });
                }
            }, {
                init: function() {

                },

                '.addCommentary submit': function (el, ev) {
                    ev.preventDefault();
                    var self = this;

                    can.ajax({
                        url: '/commentaries/addCommentary',
                        type: 'POST',
                        data: can.deparam(el.serialize()),
                        success: function (data) {
                            self.displayResponse(el, data)
                        }
                    });
                },

                displayResponse: function (el, data) {
                    var self = this;

                }

/*                '.rating .plus click': function (el, ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                    var self = this;

                    Ratings.toggleRating(el, 1, el.data('model'), el.data('component_id'), el.data('commentary_id'), self.displayRatingResponse);
                },

                '.rating .minus click': function (el, ev) {
                    ev.preventDefault();
                    ev.stopPropagation();
                    var self = this;

                    Ratings.toggleRating(el, -1, el.data('model'), el.data('component_id'), el.data('commentary_id'), self.displayRatingResponse);
                },

                displayRatingResponse: function (el, data) {
                    var commentaryId = el.data('commentary_id');

                    if (data && data.data && data.data.doc) {
                        var ratedCommentary = _.find(data.data.doc.commentaries, function(commentary){
                            return commentaryId === commentary._id;
                        });

                        if (ratedCommentary && ratedCommentary != -1) {
                            var rating = ratedCommentary.scoreInc - ratedCommentary.scoreDec;

                            el.parent().find('.number').html(rating);
                        }
                    }
                },

                '.comments_all click': function (el, ev) {
                    var $comments = el.parents('.comments');
                    var $commentsList = $comments.find('.comments_container');

                    if ($commentsList.hasClass('allVisible')) {
                        $commentsList.removeClass('allVisible');
                        $comments.find('.comments_all.all').css('display', 'inline');
                        $comments.find('.comments_all.hide').css('display', 'none');
                    } else {
                        $commentsList.addClass('allVisible');
                        $comments.find('.comments_all.all').css('display', 'none');
                        $comments.find('.comments_all.hide').css('display', 'inline');
                    }
                }*/
            });

        }
    );
})();