define([
	'canjs',
    'underscore',
	'lib/viewport',
    'core/appState'
],
	function (can, _, viewport, appState) {

		'use strict';

		function computedVal (value) {
			if (typeof value === 'function') {
				value = value();
			}
			return value;
		}

		can.mustache.registerHelper('is', function () {
			var options = arguments[arguments.length - 1],
				comparator = computedVal(arguments[0]),
				result = true;

			for (var i = 1, ln = arguments.length - 1; i < ln; i += 1) {
				if (comparator !== computedVal(arguments[i])) {
					result = false;
					break;
				}
			}

			return result ? options.fn() : options.inverse();
		});


		can.mustache.registerHelper('isnt', function (a, b, options) {
			var result = computedVal(a) !== computedVal(b);
			return result ? options.fn() : options.inverse();
		});

		can.mustache.registerHelper('or', function () {
			var options = arguments[arguments.length - 1],
				result = false;

			for (var i = arguments.length - 1; i--; ) {
				if (computedVal(arguments[i])) {
					result = true;
					break;
				}
			}

			return result ? options.fn() : options.inverse();
		});

		can.mustache.registerHelper('gt', function (a, b, options) {
			return computedVal(a) > computedVal(b)
				? options.fn()
				: options.inverse();
		});

		can.mustache.registerHelper('plus', function (a, b) {
			return computedVal(a) + computedVal(b);
		});

		can.mustache.registerHelper('minus', function (a, b) {
			return computedVal(a) - computedVal(b);
		});

		can.mustache.registerHelper('indexOfInc', function (array, element) {
			return computedVal(array).indexOf(computedVal(element)) + 1;
		});

		can.mustache.registerHelper('arrFind', function (array, id, findKey, key) {
			var item;
			id = computedVal(id);
			findKey = computedVal(findKey);
			key = computedVal(key);
			array = computedVal(array);
			if (id) {
				item = _.find(array, function (a) {
					return a.attr(findKey) === id;
				});
				if (item) {
					return item.attr(key);
				}
			}
			return '';
		});

		can.mustache.registerHelper('getArrayObjValue', function (array, index, key) {
			return array() ? array().attr(index() + '.' + key) : '';
		});

		can.mustache.registerHelper('sortedBy', function (collection, prop, direction, options) {
			if (arguments.length == 3) {
				options = direction;
				direction = false;
			}
			collection = computedVal(collection);
			if (collection && collection.attr('length') && prop) {
				var sorted = _.sortBy(collection, function (member) {
					return member.attr(prop);
				});

				if (direction && direction == 'desc') {
					sorted.reverse();
				}

				return _.map(sorted, function (member, index) {
					return options.fn(options.scope
						.add({'@index': index})
						.add(member)
					);
				}).join('');
			}
		});

		can.mustache.registerHelper('getBoxName', function (index, options) {
			var classes = ['bg-light-blue', 'bg-red', 'bg-green', 'bg-yellow', 'bg-maroon', 'bg-purple', 'bg-aqua'];
			index = computedVal(index);
			if (!index && index !== 0) {
				index = ~~(Math.random() * classes.length - 1);
			}
			return classes[index % classes.length];
		});

		can.mustache.registerHelper('wysihtml5', function (index) {
			return function (el) {
				$(el).wysihtml5();
			};
		});

		can.mustache.registerHelper('make3Col', function (index) {
			return (index() + 1) % 3 === 0 ? '<div class="clearfix"></div>' : '';
		});

		can.mustache.registerHelper('isChecked', function (value) {
			var html = '';

			if (value && value() && value() == 'on') {
				html += 'checked="checked"';
			}
			return html;
		});

		can.mustache.registerHelper('productIsActive', function (value) {
			var html = '';

			value = value();
			if (value === undefined || value === true) {
				html = 'checked';
			}

			return html;
		});

		/*****************************************************/

		can.mustache.registerHelper('renderDegustationsOptions', function (degustationTypes, relatedDegustationType) {
			var html = '';

			if (relatedDegustationType && relatedDegustationType()) {
				degustationTypes().attr().forEach(function(element, index){
					if ( typeof relatedDegustationType() == "string" ) {
						if (relatedDegustationType() == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].name + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].name + '</option>';
						}
					} else {
						if (relatedDegustationType().attr('_id') == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].name + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].name + '</option>';
						}
					}
				});
			} else {
				degustationTypes().attr().forEach(function(element, index){
					html += '<option value="' + element._id + '">' + element.lang[0].name + '</option>';
				});
			}

			return html;
		});

		can.mustache.registerHelper('renderCategoriesOptions', function (categories, relatedCategory) {
			var html = '';

			if (relatedCategory && relatedCategory()) {
				categories().attr().forEach(function(element, index){
					if ( typeof relatedCategory() == "string" ) {
						if (relatedCategory() == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].title + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
						}
					} else {
						if (relatedCategory().attr('_id') == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].title + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
						}
					}
				});
			} else {
				categories().attr().forEach(function(element, index){
					html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
				});
			}

			return html;
		});

		can.mustache.registerHelper('renderFoodOptions', function (ProductFood, productFood) {
			var html = '';

			if (ProductFood && ProductFood()) {

				if ( productFood && productFood() && productFood().attr('length')) {

					productFood().forEach(function(element) {
						var equals = _.find(ProductFood(), function (FoodElement) {
							return FoodElement._id == element;
						});

						if (equals) {
							html += '<option selected value="' + equals._id + '">' + equals.lang[0].title + '</option>';
						}
					});

					ProductFood().forEach(function(foodElement) {
						var equals = _.find(productFood(), function(element) {
							return foodElement._id == element;
						});

						if (!equals) {
							html += '<option value="' + foodElement._id + '">' + foodElement.lang[0].title + '</option>';
						}
					});

/*					ProductFood().forEach(function(foodElement, foodIndex) {
						var equals = false;

						equals = _.find(productFood(), function(element) {
							return foodElement._id == element;
						});

						if (equals) {
							html += '<option selected value="' + foodElement._id + '">' + foodElement.lang[0].title + '</option>';
						} else {
							html += '<option value="' + foodElement._id + '">' + foodElement.lang[0].title + '</option>';
						}
					});*/

				} else {
					ProductFood().forEach(function(element, index) {
						html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
					});
				}

			}

			return html;
		});

/*        can.mustache.registerHelper('renderFoodOptions', function (ProductFood, productFood) {
            var html = '';

            if (ProductFood && ProductFood()) {

	            if ( productFood && productFood() && productFood().attr('length')) {
		            ProductFood().forEach(function(foodElement, foodIndex) {
			            var equals = false;

			            equals = _.find(productFood(), function(element) {
				            return foodElement._id == element;
			            });

			            if (equals) {
				            html += '<option selected value="' + foodElement._id + '">' + foodElement.lang[0].title + '</option>';
			            } else {
				            html += '<option value="' + foodElement._id + '">' + foodElement.lang[0].title + '</option>';
			            }
		            });

	            } else {
		            ProductFood().forEach(function(element, index) {
			            html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
		            });
	            }

            }

            return html;
        });*/

		can.mustache.registerHelper('renderAuthorsOptions', function (authors, relatedAuthor) {
			var html = '<option value=""></option>';

			if (relatedAuthor && relatedAuthor()) {
				authors().attr().forEach(function(element, index){
					if ( typeof relatedAuthor() == "string" ) {
						if (relatedAuthor() == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].name + ' ' + element.lang[0].surname + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].name + ' ' + element.lang[0].surname + '</option>';
						}
					} else {
						if (relatedAuthor().attr('_id') == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].name + ' '+ element.lang[0].surname + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].name + ' ' + element.lang[0].surname + '</option>';
						}
					}
				});
			} else {
				authors().attr().forEach(function(element, index){
					html += '<option value="' + element._id + '">' + element.lang[0].name + ' ' + element.lang[0].surname + '</option>';
				});
			}

			return html;
		});

		can.mustache.registerHelper('renderVacanciesOptions', function (vacancies, relatedVacancy) {
			var html = '';

			if (relatedVacancy && relatedVacancy()) {
				vacancies().attr().forEach(function(element, index){
					if ( typeof relatedVacancy() == "string" ) {
						if (relatedVacancy() == element._id) {
							html += '<option selected="selected" value="' + element._id + '">' + element.lang[0].title + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
						}
					} else {
						if (relatedVacancy().attr('_id') == element._id) {
							html += '<option selected="selected" value="' + element.lang[0].title + '</option>';
						} else {
							html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
						}
					}
				});
			} else {
				vacancies().attr().forEach(function(element, index){
					html += '<option value="' + element._id + '">' + element.lang[0].title + '</option>';
				});
			}

			return html;
		});

		can.mustache.registerHelper('displayLangField', function (langs, field) {
			if (langs && langs() && field) {
				return langs()[0][field];
			}
		});

		can.mustache.registerHelper('getUploadedImage', function (image) {
			return appState.uploadPath + image();
		});

		can.mustache.registerHelper('checkClearNecessity', function (index) {
			var result = '';

			if ((parseInt(index()) + 1) % 4 == 0) {
				result = '<div class="clearfix"></div>'
			}
			return result;
		});

		var wysiwyg = function (slider, module, options) {
			if (options === undefined) {
				if (module === undefined) {
					options = slider;
					slider = undefined;
				} else {
					options = module;
					module = undefined;
				}
			}

			return function (el) {
				_.defer(function (slider) {
					var opts = {
						minHeight: 400,
						module: module,
						toolbar: [
							['style', ['style']],
							['font', ['bold', 'italic', 'underline', 'clear']],
							['fontsize', ['fontsize']],
							['para', ['ul', 'ol', 'paragraph']],
							['height', ['height']],
							['table', ['table']],
							['insert', ['link', 'video', 'picture']],
							['view', ['fullscreen', 'codeview']],
							['help', ['help']]
						]
					};

					if (slider) {
						opts.toolbar[8][1].splice(2, 0, 'slider');
					}
					$(el).summernote(opts);
				}, slider);
			};
		};

		can.mustache.registerHelper('wysiwyg', wysiwyg);

		can.mustache.registerHelper('colorpicker', function (index) {
			return function (el) {
				$(el).ColorPicker({
					color: $(el).val(),
					onShow: function (colpkr) {
						$(colpkr).fadeIn(500);
						return false;
					},
					onHide: function (colpkr) {
						$(colpkr).fadeOut(500);
						return false;
					},
					onChange: function (hsb, hex, rgb) {
						$(el).css('backgroundColor', '#' + hex);
						$(el).val('#' + hex);
						$(el).trigger('change');
					}
				});
			};
		});

		can.mustache.registerHelper('select2', function (index) {
			return function (el) {
				$(el).select2();
			};
		});

		can.mustache.registerHelper('datepicker', function (index) {
			return function (el) {
				$(el).datepicker({
					language: 'ru',
					weekStart: 1,
					format: 'dd.mm.yyyy'
				});
			};
		});

		can.mustache.registerHelper('translit', function (id) {

			return function (el) {

				$(el).syncTranslit({
					destination: 'link-'+id()
				});
			};
		});

/*		can.mustache.registerHelper('getDate', function(unixTimestamp) {
			var timestamp = '';

			if (unixTimestamp && typeof unixTimestamp === 'function') {

				if (unixTimestamp().length == 10) {
					timestamp = unixTimestamp();
				} else {
					var date = new Date(parseInt(unixTimestamp()));

					var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
					var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
					var year = date.getFullYear();

					timestamp = day+'.'+month+'.'+year;
				}

			} else {
				var date = new Date(unixTimestamp);

				console.log(date);

				var day = date.getDate() < 10 ? '0'+date.getDay() : date.getDay();
				var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
				var year = date.getFullYear();

				timestamp = day+'.'+month+'.'+year;
			}

			return timestamp;
		});*/

		can.mustache.registerHelper('getDate', function(unixTimestamp) {
			var timestamp = '';

			if (unixTimestamp) {
				if (typeof unixTimestamp === 'function') {

					if (unixTimestamp()) {
						if (unixTimestamp().length == 13) {
							var date = new Date(parseInt(unixTimestamp()));
						} else {
							var date = new Date(unixTimestamp());
						}

						if (date == 'Invalid Date') {
							timestamp = unixTimestamp();
						} else {
							var day = date.getDate() < 10 ? '0'+date.getDate() : date.getDate();
							var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
							var year = date.getFullYear();

							timestamp = day+'.'+month+'.'+year;
						}
					}

				} else {
					var date = new Date(unixTimestamp);

					var day = date.getDate() < 10 ? '0'+date.getDay() : date.getDay();
					var month = date.getMonth() + 1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1;
					var year = date.getFullYear();

					timestamp = day+'.'+month+'.'+year;
				}
			}

			return timestamp;
		});

		can.mustache.registerHelper('multisortable', function (id) {

			return function (el) {

				$(el).multisortable({
					items: "option",
					selectedClass: "selected",
					click: function(e){
						var $el = $(e.target);

						$el.prop('selected', !$el.prop('selected'));
					},
					stop: function(e){

					}
				});
			};
		});
	}
);
