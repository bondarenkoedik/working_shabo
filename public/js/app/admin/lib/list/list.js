define([
	'canjs',
	'core/appState'
],
	function (can, appState) {

		return can.Control.extend({
			defaults: {
				viewpath: '',
				viewName: 'index.stache',
				viewNameMore: 'more.stache',
				moreAppendTo: '.componentList',

				// Edit entity controller
				Edit: function() {},
				// Name for list entity in view
				moduleName: 'list',
				// Link to entity constructor model
				Model: can.Model,

				deleteMsg: 'Вы действительно хотите удалить эту сущность?',
				deletedMsg: 'Сущность успешно удалена',
				deletedErr: 'Ошибка удаления сущности',

				// Selectors
				add: '.add',
				edit: '.edit',
				remove: '.remove',
				toList: '.toList',
				// Selector for entity form block wrap
				formWrap: '.formWrap',
				// Entity selector wich rich with #data view helper
				parentData: '.entity',
				requestParams: {}
			}
		}, {
			init: function () {

				var self = this,
					options = self.options,
					route = can.route.attr();

				self.module = new can.Map({
					display: 'list'
				});

				self.module.attr(options.moduleName, new options.Model.List(options.requestParams));

				if (route.entity_id && route.action) {
					self.module.attr('display', 'set');
					can.when(
						self.module.attr(options.moduleName)
					).then(function () {
						self.setDocCallback(route.entity_id);
					});
				}

				self.element.html(can.view(options.viewpath + options.viewName, self.module));

			},

			/*
			 * Routes
			 */

			':module route': function (data) {
				if (data.module === this.options.moduleName) {
					this.toListCallback();
				}
			},

			':module/:action/:entity_id route': function (data) {
				if (data.module === this.options.moduleName) {
					this.setDocCallback(data.entity_id);
				}
			},

			'{add} click': function () {
				this.getComponentProperties();
/*				this.setDoc();*/
			},

			'{edit} click': function (el) {
				var options = this.options,
					doc = el.parents(options.parentData).data(options.moduleName);
				this.setDoc(doc.attr('_id'));
			},

			getComponentProperties: function () {
				var self = this;

				can.ajax({
					url: this.options.Model.resource,
					method: 'GET',
					data: {findRelatedOnly: true}
				}).done(function (data) {
					self.relatedModels = data ? data.data : false;
					self.setDoc();
				}).fail(function () {
					console.log('failed');
				});
			},

			setDoc: function (id) {
				can.route.attr({
					entity_id: id || '0',
					action: 'set'
				});
			},

			setDocCallback: function (id) {
				var self = this;

				this.module.attr('display', 'set');

				if (this.module.attr('setEntity')
					&& this.module.attr('setEntity')() === id) {
					return;
				}

				var options = this.options,
					wrap = this.element.find(options.formWrap).html('<div class="right-side"></div>'),
					area = wrap.children('div');

				this.module.attr('setEntity', can.compute(id));

				this.initSetControl(
					area,
					self.getModelInstance(id),
					this.module.attr('setEntity')
				);
			},

			getModelInstance: function (id) {
				var doc = this.getExistingInstance(id);
				if (doc) return doc;
				var self = this;
				var model =	new self.options.Model();

				if ( self.relatedModels ) {
					for ( var key in self.relatedModels ) {
						model.attr(key, self.relatedModels[key]);
					}
					self.relatedModels = false;
				}

				return model;
			},

			getExistingInstance: function (id) {
				var options = this.options;
				return _.find(this.module.attr(options.moduleName), function (doc) {
					return doc && doc.attr('_id') === id;
				});
			},

			initSetControl: function (area, doc, entity) {
				new this.options.Edit(area, {
					doc: doc,
					entity: entity
				});
			},

			'{toList} click': function () {
				can.route.attr({
					entity_id: undefined,
					action: undefined
				});
			},

			toListCallback: function () {
				this.module.attr({
					'display': 'list'
				});
			},

			'{remove} click': function (el) {
				var options = this.options,
					doc = el.parents(options.parentData).data(options.moduleName);

				if (confirm(options.deleteMsg)) {
					doc.destroy().always(function (doc, status, def) {
						appState.attr('notification', {
							status: status,
							msg: status === 'success'
								? options.deletedMsg
								: options.deletedErr
						});
					});
				}
			},

			'{Model} created': function (Model, ev, doc) {
				var self = this,
					docs = self.module.attr(self.options.moduleName);

				docs.push(doc);
			},

			'.loadMoreDocuments click': function () {
				var self = this;

				self.options.requestParams.queryOptions.skip += self.options.requestParams.queryOptions.limit;

				new self.options.Model.List(self.options.requestParams).done(function (data) {
					data.each(function(element) {
						self.module.attr(self.options.moduleName).push(element);
					});
				});
			}
		});
	}
);
