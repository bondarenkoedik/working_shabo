'use strict';

(function(){

	var component = 'articleCategory',
		componentUppercase = 'ArticleCategory',
		componentPlural = 'articleCategories';

	define(
		[
			'canjs',
			'lib/edit/edit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранено.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
