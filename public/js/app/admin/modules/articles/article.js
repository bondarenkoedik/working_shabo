'use strict';

(function(){

	var component = 'article',
		componentUppercase = 'Article',
		componentPlural = 'articles';

	define(
		[
			'canjs',
			'lib/edit/edit',
			'colorpicker',
			'datepicker',
			'translit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранен.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {
				'{form} submit': function (el, ev) {
					ev.preventDefault();

					var self = this,
						options = self.options,
						data = this.getDocData(el),
						doc = options.doc;

					doc.attr(data);

					if (doc.author == null || doc.author.length == 0) {
						delete doc.author;
						delete doc._data.author;
					}

					doc.save()
						.done(function(doс) {
							options.entity(doc.attr('_id'));
							if (options.setRoute) {
								can.route.attr({'entity_id': doc.attr('_id')});
							}
							self.setNotification('success', options.successMsg);
						})
						.fail(function (doc) {
							self.setNotification('error', options.errorMsg);
						});

				}
			});

		}
	);

}());
