'use strict';

(function(){

	var component = 'vacancyResponse',
		componentUppercase = 'VacancyResponse',
		componentPlural = 'vacancyResponses';

	define(
		[
			'canjs',
			'lib/edit/edit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранен.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
