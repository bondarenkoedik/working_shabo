'use strict';

(function(){


	var component = 'country',
		componentUppercase = 'Country',
		componentPlural = 'countries';

	define(
		[
			'canjs',
			'core/appState',
			'lib/edit/edit',
			'colorpicker'
		],

		function (can, appState, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранено.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {

				init: function () {
					var options = this.options,
						data = {
							langs: appState.attr('langs')
						};
					data[options.moduleName] = options.doc;

					this.element.html(can.view(options.viewpath + options.viewName, data));
					this.fillMap(data);

					var mapPointer = document.getElementById('map-pointer');
					var mapWrapper = $('#map');
					var parentOffset = mapWrapper.offset();

					mapPointer.onmousedown = function(e) {

						var shiftX = e.pageX - (e.pageX - parentOffset.left - 10);
						var shiftY = e.pageY - (e.pageY - parentOffset.top - 29);

						moveAt(e);

						function moveAt(e) {

							var relX = e.pageX - parentOffset.left - 10;
							var relY = e.pageY - parentOffset.top - 29;

							mapPointer.style.left = e.pageX - shiftX + 'px';
							mapPointer.style.top = e.pageY - shiftY + 'px';


							$('#pointerTop').val(relY);
							$('#pointerLeft').val(relX);
						}

						document.onmousemove = function(e) {
							moveAt(e);
						};

						document.onmouseup = function() {
							document.onmousemove = null;
							mapPointer.onmouseup = null;
						};

					};

					mapPointer.ondragstart = function() {
						return false;
					};
				},

				fillMap: function ( data ) {

					$('#'+data.country.mapId).css('fill', data.country.color);
					$('#'+data.country.mapId).find('path').css('fill', data.country.color);
				},

				'path click': function (el, ev) {
					var id = '';
					var isPath = false;

					id = $(el.parent()).attr('id');

					if (!id) {
						id = $(el).attr('id');
						isPath = true;
					}

					var mapWrapper = $(el).parents('#map');

					var parentOffset = mapWrapper.offset();

					var relX = ev.pageX - parentOffset.left - 10;
					var relY = ev.pageY - parentOffset.top - 29;

					mapWrapper.find('.map-pointer').css({
						top: relY,
						left: relX
					});

					$('#pointerTop').val(relY);
					$('#pointerLeft').val(relX);

					$('input[name=mapId]').val(id);

					var mapColorValue = $('.map-color').val();
					if ( mapColorValue.length > 0 ) {

						if (isPath) {

							var rgbArray = $('#'+id).css('fill')
								.replace(/[()]/g, '')
								.replace(/rgb/g, '')
								.replace(/\s/g, '')
								.split(',');

							if (this.rgbToHex(rgbArray[0], rgbArray[1], rgbArray[2]) == mapColorValue) {
								$('#'+id).css('fill', '#d9b598');
							} else {
								$('#'+id).css('fill', mapColorValue);
							}

						} else {
							var rgbArray = $('#'+id).find('path').css('fill')
								.replace(/[()]/g, '')
								.replace(/rgb/g, '')
								.replace(/\s/g, '')
								.split(',');

							if (this.rgbToHex(rgbArray[0], rgbArray[1], rgbArray[2]) == mapColorValue) {
								$('#'+id).find('path').css('fill', '#d9b598');
							} else {
								$('#'+id).find('path').css('fill', mapColorValue);
							}
						}
					}
				},

				'.map-pointer dragstart': function (el, ev) {
					return false
				},

				// '.map-pointer mousedown': function (el, ev) {
				// 	console.log('drag');
				// },

				componentToHex: function (c) {
					var hex = parseInt(c).toString(16);
					return hex.length == 1 ? "0" + hex : hex;
				},

				rgbToHex: function (r, g, b) {
					return "#" + this.componentToHex(r) + this.componentToHex(g) + this.componentToHex(b);
				},

				'.map-color change': function (el, ev) {
					var groupId = $('input[name=mapId]').val();
					$('#'+groupId).css('fill', $(el).val());
					$('#'+groupId).find('path').css('fill', $(el).val());
				}
			});

		}
	);

}());
