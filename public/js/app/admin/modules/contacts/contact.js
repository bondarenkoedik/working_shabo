'use strict';

(function(){

	var component = 'contact',
		componentUppercase = 'Contact',
		componentPlural = 'contacts';

	define(
		[
			'canjs',
			'lib/edit/edit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранен.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
