'use strict';

(function(){

	var component = 'degustation',
		componentUppercase = 'Degustation',
		componentPlural = 'degustations';

	define(
		[
			'canjs',
			'lib/edit/edit',
			'translit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранено.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {});

		}
	);

}());
