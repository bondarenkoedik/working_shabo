'use strict';

(function(){

	var component = 'productCategory',
		componentUppercase = 'ProductCategory',
		componentPlural = 'productCategories';

	define(
		[
			'canjs',
			'lib/edit/edit'
		],

		function (can, Edit) {

			return Edit.extend({
				defaults: {
					viewpath: 'modules/'+componentPlural+'/views/',

					moduleName: component,

					successMsg: 'Успешно сохранено.',
					errorMsg: 'Ошибка сохранения.',

					form: '.set'+componentUppercase
				}
			}, {
				'{form} submit': function (el, ev) {
					ev.preventDefault();

					var self = this,
						options = self.options,
						data = this.getDocData(el),
						doc = options.doc;

					if (self.checked !== undefined) {
						data.active = self.checked;
					}

					doc.attr(data);

					doc.save()
						.done(function(doс) {
							options.entity(doc.attr('_id'));
							if (options.setRoute) {
								can.route.attr({'entity_id': doc.attr('_id')});
							}
							self.setNotification('success', options.successMsg);
						})
						.fail(function (doc) {
							self.setNotification('error', options.errorMsg);
						});

				},

				'#product-category-active click': function (el, ev) {
					var self = this,
						options = self.options,
						data = this.getDocData(el),
						doc = options.doc;

					self.checked = $(el).prop('checked');
				}
			});

		}
	);

}());
