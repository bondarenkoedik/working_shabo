const fs = require('fs');
var recursive = require("recursive-readdir");

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');

const largeFiles = [];
let totalSize = 0;

recursive("/Users/eduard/Documents/work/shabo.ua/public/uploads", function (err, files) {
  
  files.forEach(file => {
    const stats = fs.statSync(file);
    const size = stats.size / 1000000.0;
    if (size > 1.5) {
      largeFiles.push(file);
      console.log(file);
      totalSize += size;
    }
  });

  imagemin(largeFiles, './out/', {
    plugins: [
      // imageminJpegtran({ progressive: true }),
      // imageminPngquant({ quality: '10-20' })
    ]
  })
  .then(files => {
    console.log(files);
  });

  console.log('The amount of large files: ', largeFiles.length);
  console.log('Total size: ', totalSize);
});
