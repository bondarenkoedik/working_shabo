module.exports =
  ourWines: 'Наши вина'
  advices: 'Tips about<br> wine'
  cultureCentre: 'Wine culture center Shabo'
  wineMap: ' The wine map<br> of the world'
  news: 'News of<br> wine industry'
  productionCapacity: 'Production'
  privileges: 'Advantages<br> of Shabo Club'
  experts: 'Expert<br> Columns'
  city: 'City'
  postAddress: 'Mail address'
  postIndex: 'Post code'

  ourWinesBtn: 'View All'
  advicesBtn: 'Read'
  cultureCentreBtn: 'Start the journey'
  newsBtn: 'Go to the feed'
  expertsBtn: 'Read'

  recommendations: 'Recommended'
  consumption: 'serving'
  storage: 'storage'
  to: 'to'

  exportMarkets:
    title: 'Export markets'
    usa: 'USA'
    canada: 'Canada'
    israel: 'Israel'
    georgia: 'Georgia'
    china: 'China'
    japan: 'Japan'
    ukraine: 'Ukraine'
    estonia: 'Estonia'
    latvia: 'Latvia'
    lithuania: 'Lithuania'

  vacancyFileUnallowed:
    title: 'Check file extension'
    content: ''

  loading: 'Loading...'
  fileSent: 'File sent'

  learnMore: 'Learn more'

  company: 'Company'
  production: 'Collections'
  contacts: 'Contact Information'
  mainPage: 'Main page'
  wineNews: 'News of wine industry'
  clubPrivileges: 'Advantages of Shabo Club'
  login: 'Login'
  mission: 'Mission'
  history: 'History'
  wineyards: 'Vineyards'
  vacancies: 'Career opportunities'
  productionProcess: 'Production'
  awards: 'Awards'
  catalogue: 'Сatalogue'

  missionBtn: 'Learn more'
  historyBtn: 'Read'
  vacanciesBtn: 'Submit resume'
  productionProcessBtn: 'Learn more'
  wineyardsBtn: 'Look'
  awardsBtn: 'Look'

  copyright: 'Shabo LLC'
  joinSocial: 'Join us<br> in social networks'

  history:
    history: 'History'
    anticTimes:
      title: 'ANCIENT PERIOD'
      content: 'Shabo is one of the oldest terroirs in Europe. Ancient Greeks are considered the forefathers of winemaking in this region. In the 6th century BC, Greek colonists founded the settlement of Tyras on the Black Sea shore (later known as Akkerman) and planted the first vineyards there. It happened no less and no more than 2.5 thousand years ago…'
    turkishPeriod:
      title: 'OTTOMAN PERIOD'
      content: 'The Ottoman period in this land began in the 16th century. The settlement was renamed to Aşa-abag, Turkish for ‘lower gardens’ (as vineyards were called back then). This name was given for a reason: the vineyards were located below Akkerman (present-day Bilhorod-Dnistrovskyi).'
    turkishPeriod2:
      title: ''
      content: 'Various grape varieties were grown there, but there was one among them, which is still grown in Shabo and considered autochthonous: Telti-Kuruk, translated from Turkish as ‘fox tail’. Shabo has a special program of preserving these unique grapevines.'
    swissColonists:
      title: 'SWISS IMMIGRANTS'
      content: 'The beginning of cultural winemaking in this region is attributed to Swiss immigrants who founded here a winemaking settlement in 1822.
After a long journey of 2.5 thousand kilometers, the Swiss from the faraway Canton of Vaud have arrived to Aşa-abag on the shore of the Dniester Estuary. Because of the difficult pronunciation, the Swiss have soon renamed their settlement – first to Shabag and then to Shabo.'
    swissColonists2:
      title: ''
      content: 'The founder of the Swiss colony, Louis Tardan, wrote: ‘If you want to see the paradise on Earth, you won’t find a better place’. Thanks to the unique properties of local terroir and the exceptionally hardworking attitude of the Swiss immigrants, this area has gradually become a real grape growing country. '
    swissColonists3:
      title: ''
      content: 'The Swiss brought European winemaking standards with them and planted these standards here. As a result, in 1847 Shabo wines won their first gold medal, thus originating a tradition of winning the highest awards at the world’s best tasting contests.'
    sovietAge:
      title: 'SOVIET PERIOD'
      content: 'During Soviet times, our company specialized in primary winemaking and was one of the largest in the Soviet Union. Shabo vineyards provided the base for scientific research. The company was growing grapes of many varieties and producing base wine for still and sparkling wines; vintage wines were highly popular, too. '
    sovietAge2:
      title: ''
      content: 'Despite the best efforts of Mikhail Gorbachev’s ‘anti-alcohol campaign’, the locals were able to preserve the grape plantations and winemaking infrastructure. Nevertheless, during the years of perestroika the winemaking in Shabo has gradually declined…'
    newAge:
      title: 'NEW ERA OF WINEMAKING'
      content: 'Shabo’s centuries-old winemaking traditions were at the basis of foundation in 2003 of Shabo Wine Company, a Ukrainian winemaking complex with full production cycle. The company is engaged in grape growing, processing, production, and sales of Shabo alcoholic beverages made from selected grapes only.'
    newAge2:
      title: ''
      content: 'In a short time, the company became one of the industry’s leaders. Today, wide range of Shabo beverages is sold in all regions of Ukraine and in Georgia, Israel, China, Poland, Latvia, Estonia, Belgium, France, Denmark, Czech Republic, USA, Brazil, Canada, Azerbaijan, Japan, Iraq, Norway and Great Britain.'

    desktop:
      period1: 'Ancient<br> period'
      period2: 'Ottoman<br> period'
      period3: 'Swiss<br> period'
      period4: 'Soviet<br> period'
      period5: 'Our<br> days '
    tablet:
      period1: 'Ancient period'
      period2: 'Ottoman period'
      period3: 'Swiss period'
      period4: 'Soviet period'
      period5: 'Our days'

  productionProcessWrap:
    1:
      title: ''
      content: 'Our company produces Shabo-branded noble beverages at own production facilities located in the immediate proximity of Shabo vineyards.
In terms of equipment quality, our production process and technologies are among the best in Europe. Loyalty to traditions, combined with advanced technologies of winemaking, help create Shabo alcoholic beverages conveying the best characteristics of terroir imbued in selected grape fruits.'
    2:
      title: 'GRAPE RECEIPT AND PROCESSING FACILITY'
      content: 'Within 15 minutes after harvesting, the grapes are delivered in an ideal condition to the 1300-sq.m grape receipt and processing facility, which can process up to 20 thousand tons of red and white grapes per season. The grape receipt and processing system is fully automated. State-of-the-art, high-tech equipment maintains continuous temperature control and a sparing processing mode. '
    3:
      title: ''
      content: 'Red and white grape varieties require use of different technologies. A technology of stem separation without crushing grape fruits and a delicate pressing process, which prevents the rubbing of seeds and skin, are used in the production of white wines, while the ‘red method’ of production involves separation of stems and grape fruit crushing. '
    4:
      title: 'ELITE SHABO WINE SECTION'
      content: 'This section is where Great Shabo CDO (controlled designation of origin) Wines are created. Shabo is the first and so far, the only company is Ukraine licensed to produce CDO wines, a Ukrainian analog of French АОС (Appellation d’origine contrôlée). New-generation equipment and the skills of Shabo winemakers guarantee exclusive approach to literally every grape fruit.'
    5:
      title: ''
      content: 'Immediately after harvesting, grapes undergo double sorting at the elite wine section. After that, red variety grapes are sent to fermentation ‘on pulp’ in Taransaud oak casks equipped with a temperature control system. Without prior crushing, white variety grapes are gently pressed first at Bucher XPlus press machines and then undergo fermentation process in oak barrels with controlled temperature.'
    6:
      title: 'DISTILLED GRAPE BEVERAGE PRODUCTION COMPLEX'
      content: 'Shabo has been producing brandy since 2005 and grape vodka since 2013.
A state-of-the-art, 10,000-sq.m complex features the full distilled grape beverage production cycle. Technological scheme of full cycle includes: grape growing and processing, production of high-quality base wine, distillation of grape alcohol, aging of brandy spirit, and bottling of noble beverages: Shabo brandies and grape vodkas.'
    7:
      title: ''
      content: 'Distillation is one of the most important phases in the production of hard grape liquors. Our distillation section features a continuously operated still with distillation capacity of up to 20 million liters of base wine, enough to produce almost 3 million liters of high-quality brandy spirits with extra fine flavor and unsurpassed range of tastes.
Brandy spirit aging and storage section is equipped in accordance with European standards. Shabo brandy spirits are aged and stored in oak barrels and special tanks. The aging process lasts from three years up, depending on the category of beverage. '
    8:
      title: 'SHABO SPARKLING WINE HOUSE'
      content: 'First sparkling wines were produced in Shabo by Swiss immigrants back in the 19th century. Being the standard of quality, these wines were awarded numerous medals at international contests. Today, Shabo sparkling wines personify the experience of Swiss immigrants and revival of old traditions.
Shabo Sparkling Wine House is located in a picturesque place among Shabo vineyards, occupying the area of almost 1.5 hectares. Advanced equipment helps ensure impeccable quality at all stages of production process – from the processing of truly Champagne grape varieties to the bottling of Shabo sparkling wines. The skills of our winemakers help create sparkling wines according to a classical technology of champagnization in a bottle and the Charmat method. '
    9:
      title: ''
      content: 'The classic (aged) sparkling wine production process takes from 9 months to 3 years. During this time, a mysterious process of natural fermentation occurs in every separate bottle.'
    10:
      title: ''
      content: 'The Charmat method is also based on natural fermentation as the traditional method of champagnization in a bottle, with the only difference being that the role of bottle is played by modern closed acratophores (reservoirs) with the capacity of 20 thousand champagne bottles. That’s why these sparkling wines are sometimes called acratophoric or reservoir, and it takes 3-6 months to create them.
All sparkling wine production processes at Shabo are controlled by enologists from the Institut Oenologique de Champagne, France.'
    11:
      title: 'SHABO WINE AGING AND STORAGE'
      content: 'The 10,000-sq.m Shabo wine storage is one of the largest in Ukraine and has three levels: ground, 5 meters and 9 meters below the ground. It can simultaneously store over 10,000,000 liters of wine in oak casks and state-of-the-art stainless steel reservoirs.'
    12:
      title: ''
      content: 'Our company has a total of over 2000 French oak casks thoroughly selected, in terms of capacity, quality and region of origin of wood, for every particular varietal of Shabo wines.'
    13:
      title: ''
      content: 'To store base wine, we also use advanced vertical stainless steel reservoirs, which have a thermal ‘jacket’ to maintain constant storage temperature. '
    14:
      title: 'CHEMICAL AND TECHNICAL CONTROL LABORATORY '
      content: 'Shabo has implemented a European model of ‘continuous quality monitoring’ from grapes to finished products. Our chemical and technical control laboratory performs the most accurate measurements, fast and of any degree of complexity.'
    15:
      title: ''
      content: 'The new-generation laboratory equipment allows for application of methodologies based on Ukrainian, European and International Organization of Vine and Wine (OIV) standards.'
    16:
      title: 'BOTTLING DEPARTMENT'
      content: 'Every year, Shabo produces almost 50,000,000 bottles of over 80 brand names of Shabo beverages created from selected grapes only. Our company has 9 bottling lines featuring state-of-the-art European-manufactured equipment, which allows to fully preserve the taste and flavor of freshly-harvested grapes and their priceless natural properties in Shabo beverages during bottling.'

    period1: 'Introduction'
    period2: 'Grape receipt<br> and processing facility'
    period3: 'Elite Shabo<br> wine section'
    period4: 'Distilled grape beverage<br> production complex'
    period5: 'Shabo Sparkling<br> Wine House'
    period6: 'Shabo wine aging<br> and storage'
    period7: 'Chemical<br> and technical<br> control laboratory'
    period8: 'Bottling<br> department'

    tablet:
      period1: 'Introduction'
      period2: 'Grape receipt<br> and processing facility'
      period3: 'Elite Shabo<br> wine section'
      period4: 'Distilled grape beverage<br> production complex'
      period5: 'Shabo Sparkling<br> Wine House'
      period6: 'Shabo wine aging<br> and storage'
      period7: 'Chemical<br> and technical<br> control laboratory'
      period8: 'Bottling<br> department'

  cultureCentreDescription: 'About wine culture center Shabo'
  cultureCentreDescriptionBtn: 'Read'

  tours: 'Tours'
  toursBtn: 'Learn more'

  gallery: 'Gallery'
  galleryBtn: 'Look'

  videos: 'Video'
  videosBtn: 'Watch'

  degustations: 'Tasting'
  degustationsBtn: 'Learn more'

  feedback: 'Feedback'
  feedbackBtn: 'Read'

  photos: 'Gallery'

  tourScheduleHours: 'Excursion schedule:<br>
	10:00, 13:00, 16:00, 18:00'
  tourScheduleContent: 'Visit us and enjoy the life of vine from a seed to the last drop on the bottom of a glass. <br>
	Duration of excursion – 2 hours <br>
	More information? <br>
	Call us and we will be happy to answer your questions on telephone number: '
  tourSchedulePhone: '+38(048)7000-210'
  orderTour: 'Order an excursion'

  degustationContent:
    header: 'TASTING ASSORTMENT FOR 2017'
    standard:
      title: 'TASTING CARTE FOR STANDARD TOURS'
      snack: 'Tasting snack:'
      hrn: 'uah'
      img: 'Shabo_degustation1.png'
      list: '<li data-index="1">1. Shabo Classic Chardonnay, varietal white dry table wine</li>
			<li data-index="2">2. Shabo Classic Cabernet, varietal red dry table wine</li>
			<li data-index="3">3. Shabo Classic White Semisweet, varietal white semisweet table wine</li>
			<li data-index="4">4. Shabo Classic Red Semisweet, varietal red semisweet table wine</li>
			<li data-index="5">5. Shabo Muscat, white semisweet sparkling wine</li>
			<li data-index="6">6. Shabo Classic Bianco, white dessert vermouth</li>
			<li data-index="7">7. Shabo V.S., ordinary Ukrainian brandy three stars</li>
			<li data-index="8">8. Shabo Proba No 1, grape vodka</li>'

    vip:
      title: 'TASTING CARTE FOR VIP TOURS'
      snack: 'Tasting snack: crackers, cheese'
      img: 'Shabo_degustation2.png'
      list:   '<li data-index="1">1. Shabo Reserve Chardonnay, aged varietal white dry table wine</li>
			<li data-index="2">2. Shabo Reserve Cabernet, aged varietal red dry table wine</li>
			<li data-index="3">3. Shabo Classic Sherry, fortified special-type white dry wine</li>
			<li data-index="4">4. Shabo Classic Merlot, varietal red dry table wine</li>
			<li data-index="5">5. Shabo Classic Brut, aged white sparkling wine</li>
			<li data-index="6">6. Shabo Х.О., grape brandy 10 y.o.</li>
			<li data-index="7">7. Grape Vodka Shabo, grape vodka</li>'

  vacanciesDescription: 'Если для вас вино не увлечение, а настоящая профессиональная страсть, то мы, компания «Шабо», будем рады видеть вас в нашем теплом коллективе. Отправьте резюме на вакансию и, возможно, в скором времени вы станете частью команды высокопрофессиональных сотрудников.'
  respondToVacancy: 'SUBMIT RESUME'

  expertsColumn: 'Expert columns'
  readWholeArticle: 'Read'

  map:
    parallel47: '47 parallel'
    france:
      title: 'France'
      content: 'France gave the world some of the most important winemaking terms: ‘terroir’ and ‘appellation’, famous grape varieties (Cabernet-Sauvignon, Merlot, Syrah, Chardonnay, Sauvignon Blanc and Pinot varieties), and exemplary wines.
The origins of winemaking in France date back to the 6th century BC, and today, it became one of the country’s calling cards. France’s vineyards occupy almost 900 thousand hectares of land, and every year, French winemakers produce some 8 billion bottles of wine. France is legislatively divided into 16 winemaking regions (including Cognac, Armagnac, and Calvados), and the best known of them are: Bordeaux, Burgundy, Rhone Valley, Loire Valley, Languedoc-Roussillon, Champagne, and Alsace. Each of these regions has an unparalleled terroir and unique varieties. That explains the great diversity of French wines.
France has the most complex wine classification system in the world and was the first to introduce standardization, which subsequently was borrowed by other winemaking countries. Moreover, the French approach to classification of wines and vineyards provided the basis for the universal classification system approved by the European Union.'
    italy:
      title: 'Italy'
      content: 'Italy has centuries-old winemaking history. Today’s winemaking Italy is comprised of 20 regions and 521 wine-producing areas. The country produces over 2500 brand names of wine, so almost every city and town in Italy has something to be proud of.
As for the grape varieties - Nebbiolo, Sangiovese, and Nero d’Avola are considered the most famous autochthonous Italian varieties in the world. In addition, experts praise a number of other varieties, including Corvina, Cortese, Molinara, Rondinella, Glera, Canaiolo, Trebbiano, Malvasia Nera, and others. Italy has a four-tier classification system: DOCG – Denominazione di Origine Controllata e Garantita (controlled designation of origin guaranteed), DOC – Denominazione di Origine Controllata (controlled designation of origin), IGT – Indicazione Geografica Tipica (typical geographical indication) used to label high-quality local wines, and VdT – Vino da Tavola (table wine) used to label the simplest table wines.'
    spain:
      title: 'Spain'
      content: 'Systematic cultivation of grape in Spain began in the 8th century BC when Greeks first colonized this area. Today, Spain, along with France and Italy, is ranked among the top three wine producers in Europe and first in terms of the total vineyard area (1.1 million ha). Spain has the total of 17 wine-producing regions, with Andalucía, Catalonia, Rioja, La Mancha and Castile and León being the most famous. Native Spanish grape varieties are little known and not among the most widely planted. The only exceptions are Albariño, a white grape variety also cultivated in Portugal, and Macabeo, which can also be found in California, Italy, and Morocco. Garnacha enjoys certain respect among French winemakers, but the most commonly known and cultivated Spanish variety outside the country is red Tempranillo. In total, over 300 grape varieties are grown in Spain.
Spanish wines are labeled according to a two-tier classification: VdM (Vinos de Mesa, or table wines) and VCPRD (Vino de Calidad Producido en Región Determinad, or Quality Wine Produced in a Specific Region; this tier also includes DO (Denominación de Origen) and DOCa (Denominación de Origen Calificada)); in turn, each of the two labeling groups has several quality levels.'
    germany:
      title: 'Germany'
      content: 'In Germany, vineyards are planted along the Rhine and Mosel Rivers on the sun-drenched banks rich in shale and basalt, two minerals that excellently retain the warmth. Local climate is favorable for the production of high-quality white wines notable for the perfect balance of acidity and sweetness. Therefore, not surprisingly, white varieties are grown on over 80% of the total area of Germany’s vineyards (occupying over 100 thousand hectares of land). Among the most cultivated varieties are, first of all, Riesling, Müller-Thurgau, and Silvaner, while Spätburgunder (German for Pinot noir) is the most popular among red varieties.
Germany has 13 legislatively designated winemaking regions, including Mosel, Palatinate, Baden, Ahr, Rheingau, and others. The German wine classification is based not on the terroir but on the sugar content due to climate considerations. The QmP (Qualitätswein mit Prädikat, or quality wine with specific attributes) level includes 6 categories depending on the grape ripening level (in order of increasing sugar levels in the must): Kabinett, Spätlese, Auslese, Beerenauslese, Eiswein, and Trockenbeerenauslese. '
    austria:
      title: 'Austria'
      content: 'We still have little knowledge of winemaking Austria, even though the country’s wine production history dates back to the 7th century BC. Over the centuries of turbulence, which saw scandals and declining reputation, today’s Austria shows excellent results. Yes, it isn’t the largest point on the wine map, but that cannot diminish its merits.
Austria has four wine-producing regions: Vienna, Burgenland, Styria, and Lower Austria (Weinviertel, Kamptal, Kremstal, Wachau and Traisental). Overall, wine production is concentrated in the country’s eastern and southeastern parts where white Grüner Veltliner, Welschriesling and Müller-Thurgau and red Blaufränkisch, St. Laurent and Zweigelt are the leading grape varieties. In total, Austrians cultivate 22 white and 13 red grape varieties grown on some 51 thousand hectares of land.
Climate in Austria is not unlike in Burgundy, so grape varieties resemble Slovakian and quality labels are similar to Germany’s where sugar content is the determining criterion. In 2014, Austria has introduced a three-tier quality designation scheme for sparkling wines.'
    portugal:
      title: 'Portugal'
      content: 'Portugal is the birthplace of Port wine and Madera, unusual green wines, fine white dry, and high-acid red wines. The Portuguese boast a rich range of autochthonous varieties: Baga, Trincadeira, Aragonez, Jaen, Ramisco, Alicante Bouschet, Touriga Francesa, and many others. Vineyards in Portugal occupy over 300 thousand hectares of land. Wine-producing regions include Vinho Verde, Algarve, Bairrada, Alentejo, Dão, Bucelas, Douro, Colares, Madeira, Tejo and Lisbon. As for classification, Portuguese wines were until recently labeled according to age: young and mature. Today’s appellation system is close to French and is based on the terroir principle. Labeling tiers include: DOC (Denominação de Origem Controlada), the supreme category that includes 29 labels, followed by IPR (Indicação de Proveniência Regulamentada, similar to French VDQS), Vinho Regional (the same as French VDP regional wines), and finally Vinho de Mesa (table wine). '
    china:
      title: 'China'
      content: 'The history of winemaking in China is 5 thousand years old. However, the modern breakthrough has occurred only in the late 20th century, and today, China is considered one of the world’s leading wine producers. Thus, in 2012 the industry has generated $1.55 billion in revenues.
The seriousness of intentions and processes are best manifested in the names of companies investing in China’s wine production industry: Rémy Martin was the first, soon followed by the Torres family and Beijing Friendship, which includes Pernod Ricard. Today, Europeans continue to develop Chinese land where, in addition to native varieties, they successfully cultivate European varieties including Riesling, Chardonnay, Cabernet-Sauvignon, and others. The best grape growing location is Shandong peninsula in southern China. Nevertheless, the main flow of table wines comes from Xinjiang, an autonomous region in the northwest of the country, but the winters there are so cold that the vineyards have to be blanketed. Overall, China has the so-called ‘wine belt’, which includes five main wine-producing regions: Hebei, Shanxi, Yantai-Penglai, Helan Mountains and Xinjiang Tian Shan.'
    moldova:
      title: 'Moldova'
      content: 'For decades, winemaking has been one of Moldova’s priority sectors of economy, and today, 95% of the country’s wine production output is exported. In the past, the key sales market was Russia, but today, the country is reorienting itself toward Europe and Asia.
Moldova has four wine-producing regions: Northern (Balti), Central (Codru), Southern (Cahul), and Southeastern (Nistreana). The winemaking potential of this area has been fully revealed since the early 19th century by French and German colonists. Vineyards occupy the total of 147 thousand hectares of land where, in addition to traditional international varieties, Moldova’s autochthonous varieties, such as Fetească Albă, Fetească Regală, and Rara Neagră, are cultivated.
The country’s largest region is Codru (87 thousand ha), where vineyards are planted among wooded hills. The southern region supplies base wine for sparkling, dessert and red dry wines, while Balti specializes in fortified wines.'
    ukraine:
      title: 'Ukraine'
      content: 'Winemaking in Ukraine has been historically predetermined: our country has advantageous geographical location, and its climate favors grape growing. Not surprisingly, during the Soviet era Ukraine was the main wine producer. The origins of winemaking in our country date back to the 4th century BC (in Crimea), going 2500 years back in the present-day Odesa Region.
Ukraine’s main wine-producing regions include Odesa (52 thousand ha of vineyards), Kherson (20 thousand ha), Mykolaiv (15 thousand ha) and Transcarpathian (8 thousand ha) Oblasts and Crimea Additionally, small vineyards are planted in other regions of Ukraine. The varietal diversity of grapes in Ukraine is represented by the internationally selected and also several autochthonous varieties. The most renowned autochthonous variety is Telti Kuyruk cultivated on in Shabo terroir. The most widely planted selected variety is Odesa Black and Sukhyi Liman White, while Saperavi, Aligote, Rkatsiteli, Cabernet-Sauvignon, Chardonnay, and Riesling are the most popular international varieties.'
    usa:
      title: 'United States'
      content: 'Until certain point, the main achievement of American winemakers was a solution of the problem of phylloxera which struck Europe: phylloxera-resistant American rootstock was grafted to susceptible European vines. Today, thanks to the successful enological research, American winemaking is ranked at par with the Old World in terms of both quality and sales turnover.
Vineyards in the United States occupy over 408 thousand hectares of land, including over 355 thousand ha in California alone (the rest are in New York, Washington, Michigan, Pennsylvania and Oregon). The most widely planted international varieties there are Zinfandel, Pinot noir, Merlot, Syrah, Cabernet-Sauvignon, Cabernet Franc, Chardonnay, Sauvignon Blanc, Riesling, and Viognier.
The United States has designation system called the American Viticultural Area (AVA), which includes 202 regions. The division is based on terrain and climatic conditions. '

  howTovisitDegustation: 'Tasting assortment'
  orderDegustation: 'Order the tickets and ask all the questions you have on the telephone number: <br>
	+38(048) 7000-210'

  toProduction: 'К продукции'

  surname: 'Surname'
  name: 'Name'
  phone: 'Phone'
  email: 'Email'
  commentary: 'Commentary'
  ready: 'Ready'
  addFile: 'Upload CV'
  cv: 'Resume'
  vacancyResponseDescription: 'To send form you need to fill next fields: surname, name, patronymic, phone, cv file.'
  fileAdded: 'File added'

  vacancyResponseSuccess:
    title: 'Thank you!'
    content: 'If you match our criteria we will contact you.'
  vacancyResponseFailure:
    title: 'Error!'
    content: 'An error occurred while sending your response. Reload page and try again.'

  yes: 'Yes'
  no: 'No'
  warningText: 'Attention! The website contains information<br> not recommended for under age persons.'
  warningQuestion: 'Are you of full age?'

  toNewsList: 'Back to feed'

  sendFeedback: 'Submit your review'
  moreFeedback: 'More reviews'
  toCollectionList: 'To the list of collections'

  awardsDescription: 'Each year Shabo company succeeds in professional, production and social spheres that is proved by numerous honors and awards:'

  faqs: 'FAQ'
  loadMoreAnswers: 'more'

  registration: 'Sign up'
  agreedWithRules: 'Accept the <span class="rulesPopup">rules</span>'
  orRegister: 'Or sign up'
  firstName: 'Name'
  lastName: 'Family name'
  password: 'Password'
  authorization: 'Sign in'
  successfulRegistration: 'Registration successful'
  registrationSuccessText: 'To complete your registration follow the link in the email, please.'

  country: 'Country'
  getDiscount: 'Receive a discount'
  getDiscountDescription: 'To receive a discount for a visit to Shabo Wine Culture Center fill in the below fields'
  logout: 'Logout'
  back: 'Back'
  loginSuccess: 'You logged in successfully'
  registrationConfirmationSuccess: 'We are happy to welcome you in Shabo Club'
  forgotPassword: 'Forgot password?'
  forgotPasswordText: 'Please enter the email address used during registration. The new password will be sent to the indicated email.'

  send: 'Send'
  moreArticles: 'More articles'
  ourExperts: 'Our experts'

  leaveCommentary: 'Leave a comment'
  askExpert: 'Ask an expert'
  ask: 'Ask'
  askExpertSuccess: 'Thank you for your question! The expert will certainly consider it. You will be informed about reply via email'
  weAreHappyYouRegistered: 'We are happy you have decided to join Shabo Club!'

  currentOffers: 'Current offers'

  authHint: 'Register or login in Shabo Club to communicate and share your opinion'
  allExperts: 'All the experts'

  articles: 'Articles'
  feedbackFormPlaceholder: 'Register or login in Shabo Club to communicate and share your opinion'
  capacity: 'volume'
  litre: 'L'
  alcPercent: 'ABV'

  winemap:
    cabernet:
      title: 'Cabernet-Sauvignon'
      description: 'Cabernet-Sauvignon is a prominent French grape cultivar considered the symbol of all red wines in the world and the ‘number one’ among the seven stars of grapevine varieties.
The quality of Cabernet-Sauvignon is profoundly affected by the terroir. In some regions, the wine becomes very soft, while in others it turns out more tart. Shabo terroir develops unique features in Cabernet wines: rich flavor and velvety taste.
Cabernet-Sauvignon variety is widely used in Shabo blends, but it attains the most majestic and wholesome ‘sound’ when used in single-varietal Cabernet wines of various Shabo collections.'
    merlot:
      title: 'Merlot'
      description: 'Merlot is a French grape cultivar and one of the seven stars of grapevine varieties. It is considered the most emotional among all red varieties, demonstrates wholesome nature and great inner power, and is distinguishable for the elegancy of style.
Merlot grows equally well in various terroirs across the world. However, in Shabo, it must be disciplined by low yield and harvesting fully ripe grapes only. Under these conditions, the grape rewards winemakers with a great wine – open, rich, and attractive.
Merlot variety is used in various Shabo blends, but its nature is revealed the most vividly in single-varietal Merlot wines of various Shabo collections.'
    pinotnoir:
      title: 'Pinot noir'
      description: 'Pinot noir is a French grape cultivar and one of the oldest varietals (dating over 2 thousand years back). It is also one of the seven stars of grapevine varieties and one of the three varieties used in the production of champagne.
This is a stubborn variety resembling a spoiled child. Pinor noir dictates own terms to the terroir. Only when the soil, climate and grapevine cultivation suit it will the winemaker be able to produce enchanting red, rose, and white wines from it. This variety feels quite comfortable in Shabo terroir, growing well and imbuing complex luring notes of various flavors and tastes.
At Shabo, Pinot noir is a key variety in the production of Shabo sparkling wines.'
    saperavi:
      title: 'Saperavi'
      description: 'Saperavi is a highly-valuable Georgian grape variety and a sort of Georgia’s visiting card in the world. Translated from Georgian, it means ‘dyer’, named so because of the abundance of colorants in its fruits.
Saperavi is a patriot variety, feeling most comfortably in native Georgian soils. To create an authentic Saperavi wine, Shabo uses the best grapes from Kakhetian vineyards owned by our company. The study of soil in Shabo terroir shows that here, in Ukrainian land this Georgian variety will produce a good yield. Therefore, we intend to plant Saperavi vineyards at Shabo in the nearest future.
Saperavi perfectly blends with other varieties in Shabo wines, but its qualities are best revealed in single-varietal Saperavi wines of various Shabo collections.'
    chardonnay:
      title: 'Chardonnay'
      description: 'Chardonnay is a French grape cultivar, a celebrity grape and one of the brightest star varieties in the world. The symbol of all white wines of the world and a gem among the top seven grapevine varieties.
Chardonnay is a very obedient variety easy to adapt to a particular terroir. Lands on all five continents give it a warm welcome, and in return, the grape rewards winemakers with an excellent white wine.
At Shabo, we produce luring, exquisite and lively white wines from this variety. A winner of numerous international contests, Shabo Chardonnay is considered a symbol of success and our company’s visiting card.
It is a key variety in the production of Shabo sparkling wines. It is also an excellent choice in the production of Shabo grape vodka and is used extensively in Shabo wine blends. However, its top performance is the solo part in single-varietal Chardonnay wines of various Shabo collections.'
    sauvignonblanc:
      title: 'Sauvignon Blanc'
      description: 'Sauvignon Blanc is a French grape cultivar ranked among the seven stars of grapevine varieties and one of the most recognizable varieties in the world.
It enjoys the widest popularity and representation in the world, but at the same time, winemakers regard it as a variety with an unpleasant character.
Shabo terroir’s natural and climatic conditions are quite benign for Sauvignon Blanc. Here, the grape excellently reveals its exciting liveliness and characteristic aroma, but only under the condition that it is harvested when the ripeness of grapes is optimal and the wine is then produced using advanced technologies.
Although the variety’s name includes the word ‘blanc’ (white), Sauvignon Blanc grape fruits are, in fact, green. That’s why the wine produced from this variety is sometimes called ‘marvelous green’.
Sauvignon Blanc is used extensively in Shabo wine blends and to produce Shabo grape vodka, but its performance is the most colorful in varietal Shabo Sauvignon Blanc wines.'
    riesling:
      title: 'Riesling'
      description: 'Riesling is a European grape cultivar and a gem among the seven stars of grapevine varieties. By origin, Riesling is a northerner and its cultivation began during the late Middle Ages.
This variety has a unique gift of telling about the soil of its terroir much more than any other grape varieties. It is like a summary of natural world, an optic lens aimed at the terroir.
It is believed that growing Riesling is a very risky task and that it’s not easy to tame this variety outside its ‘home’ terroir… Only professionals of the highest class can do it.
It is a key variety in the production of Shabo sparkling wines. The taste of Riesling in Shabo grape vodka ‘sounds’ even more convincingly than in nature itself. Riesling is also used in Shabo wine blends.'
    muscatottonel:
      title: 'Muscat Ottonel'
      description: 'Muscat Ottonel is a Western European grape cultivar, a sort of universal grape used in the production of still and sparkling wines. In fresh form, this early table variety has a respectable number of connoisseurs, too.
Muscat Ottonel, which originates from France, received broad popularity in Western Europe in the second half of the 19th century.
For Shabo winemakers, this variety is not easy to cultivate. It’s very demanding to the terroir and cultivation technologies. Considering the variety’s tenderness and sensitivity, one can produce excellent full, soft, and smooth Muscat wines with a tender nutmeg taste from it.
Muscat Ottonel is used extensively in Shabo wine blends. It is also a key variety in the production of Shabo Muscat sparkling wines and Shabo Muscat grape vodka.'
    teltikuruk:
      title: 'Telti-Kuruk'
      description: 'Telti-Kuruk is an ancient autochthonous grape variety growing in Shabo terroir, and is a pride and a gem in the collection of Shabo vineyards. Its origins date back to the ‘Ottoman period’ in the history of Shabo. Translated from Turkish, it means ‘fox tail’ because of the specific shape of cluster resembling a fox tail. Today, Shabo cultivates this variety on over 40 hectares of land.
Telti-Kuruk feels ‘at home’ on the sandy soils of Shabo terroir, growing well and pleasing winemakers with its quality and unpretentiousness. Preservation and development of this native grape variety is a social responsibility of Shabo winemakers. To preserve it, we have developed a special long-term program that we are gradually implementing.
Telti-Kuruk is an unparalleled element of Shabo wine blends, but its brightest performance is solo in the premium-class Shabo AOC varietal Telti-Kuruk wine.  '
    rkatsiteli:
      title: 'Rkatsiteli'
      description: 'Rkatsiteli is a highly-valuable Georgian grape variety inducted to the ‘golden stock’ of Georgian winemaking. This variety originates from Kakhetia. Translated from Georgian, its name means ‘red vine’. Rkatsiteli is considered a universal grape variety, a ‘Jack of all trades’. It is used to produce European- and Kakhetian-type wines which cannot be confused with anything else. Rkatsiteli shows excellent result in the production of grape alcohol used in brandies and grape vodkas. Finally, it is simply a tasty table grape from which one can make an enjoyable and healthy grape juice. Rkatsiteli is an excellent performer in the production of Shabo grape vodka, ‘sounds’ profoundly in Shabo dry sherry and is used as the basis for the blends of Shabo still wines.'

  mail:
    header:
      title: 'Wine culture center Shabo'
      description: '67770, Ukraine<br> Odessa region, Belgorod-Dnestrovskyi district, <br> Shabo Village<br> 10 Dzerzhinskyi str.'
    copyright: 'PTK Shabo Ltd.'
    joinSocial: 'Join us in social networks'
    registration:
      subject: 'Registration on site www.shabo.ua'
      thanksForInterest: 'Thank you for your interest in Shabo Club'
      toFinishRegistration: 'To complete registration follow the link below, please<br>'
      success: 'Registration in Shabo Club was successful!'
    club:
      subject: 'Dear member of “Shabo” club!'
      header: 'Dear member of “Shabo” club!'
      description: '<p>We are happy that for you wine is not only beverage but something more and you want to find out
                    more about it. That is why we give you a one-time 20 % discount for visiting of Wine Cultural Center Shabo. Coupon with
                    discount is attached to this letter. To get the discount print coupon and show it to the cashier while buying ticket
                    for excursion at Wine Cultural Center Shabo</p>'
      couponWillBeSent: 'The discount voucher will be sent to your email:'
      surname: 'Family name'
      name: 'Name'
      country: 'Country'
      city: 'City'
      postAddress: 'Postal address'
      postIndex: 'Post code'
      phone: 'Tel.'
      bottom: '<p>
                  Tickets reservation is required: tel.: +380 48 7000 210
                </p><p>
                      We look forward to meeting you!
                  </p>'
      shaboPhone: ''
      email: 'e-mail: e.kosulnikova@shabo.ua, j.sergeeva@shabo.ua'

  productCategories:
    shaboReserve: 'SHABO RESERVE'
    korolevskiyeIstorii: 'ROYAL<br> STORIES '
    igristoeReservuarnoe: 'SHABO<br> SPARKLING WINE'
    shaboClassica: 'SHABO<br> CLASSIC'
    elitnyCognac: 'SHABO ELITE<br> COGNAC OF UKRAINE'
    cognacUkrainy: 'SHABO COGNAC<br> OF UKRAINE'
    vodka: 'SHABO<br> GRAPE VODKA'
    igristoeGold: 'SHABO GOLD<br> SPARKLING WINE'
    cognacShabskiy: 'SHABSKY YOUNG<br> BRANDY OF UKRAINE'
    shaboClassic: 'SHABO CLASSIC<br> AGED SPARKLING<br> WINE'
    vermouth: 'SHABO CLASSIC<br> VERMOUTH'
    shabskiyeVina: 'SHABO WINES'
    bagAndBox: 'SHABO BAG & BOX<br> WINES'
    georgianLegend: 'Georgian<br/> Legend'
    rkaciteli: 'KAKHETIAN-TYPE<br> RKATSITELI'
    brandy: 'AGED<br> SHABO<br> BRANDY'
    molodoe: 'SHABO GREEN WINE MOLODE'
    pshenichnaya: 'Shabo<br> wheat vodka'

  loginVK: 'Login with Vkontakte'
  loginFB: 'Login with Facebook'

  getDiscountSuccess: 'Thank you for information'
  getDiscountSuccessDescription: 'Coupon with 10% discount has already been sent to your e-mail.'

  sendCV: 'Send CV'
  upload: 'Upload'
  sendCVDescription: 'The file should be *.doc or *.pdf  with the maximum resolution 15 mb'