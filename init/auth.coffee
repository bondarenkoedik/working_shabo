async = require 'async'

passport = require 'passport'
localStrategy = require('passport-local').Strategy


socialConfig = require '../meta/socialConfig'
Model = require '../lib/model'
View = require '../lib/view'

parameters =
	usernameField: 'username'
	passwordField: 'password'

passport.serializeUser (user, done) ->
	done null, user

passport.deserializeUser (user, done) ->
	done null, user

validation = (err, user, password, done) ->
	if err
		return done err
	if not user
		return done null, false, { message: 'Пользователь с таким именем не существует!' }
	if not user.validPassword password

		return done null, false, { message: 'Пароль введен неправильно.' }
	if user.status is 0
		return done null, false, { message: 'Пользователь не активирован.' }

	done null, user

facebookValidation = (err, user, password, done) ->
	return done err if err
	return done null, false, { message: 'Пользователь с таким именем не существует' } unless user

	done null, user

vkValidation = (err, user, password, done) ->
	return done err if err
	return done null, false, { message: 'Пользователь с таким именем не существует' } unless user

	done null, user


adminStrategy = (username, password, done) ->
	cb = (err, user) ->
		validation err, user, password, done
	Model 'User', 'findOne', cb, {username : username}

userStrategy = (username, password, done) ->
	cb = (err, user) ->
		validation err, user, password, done
	Model 'User', 'findOne', cb, {username : username}

facebookLocalStrategy = (username, password, done) ->
	cb = (err, user) ->
		facebookValidation err, user, password, done
	Model 'User', 'findOne', cb, {username : username}

vkLocalStrategy = (username, password, done) ->
	cb = (err, user) ->
		vkValidation err, user, password, done
	Model 'User', 'findOne', cb, {username : username}

exports.init = (callback) ->
	adminAuth = new localStrategy adminStrategy
	clientAuth = new localStrategy userStrategy
	facebookLocalAuth = new localStrategy facebookLocalStrategy
	vkLocalAuth = new localStrategy vkLocalStrategy

	passport.use 'admin', adminAuth
	passport.use 'user', clientAuth
	passport.use 'facebookLocal', facebookLocalAuth
	passport.use 'vkLocal', vkLocalAuth

	callback()
