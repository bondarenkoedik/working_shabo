module.exports = {
	baseUrl: "public/js/lib",

	urlArgs: '',
	paths: {
		cssDir: '../../css/user',
		app: '../app/user',
		lib: '../app/admin/lib',
		userLib: '../app/user/lib',
		src: '../app/user/src',
		can: 'canjs/amd/can/',
		canjs: 'canjs/amd/can',
		core: '../app/user/core',
		jquery: 'jquery/dist/jquery',
		underscore: 'underscore/underscore',
		modules: '../app/admin/modules',
		components: '../app/admin/components',
		viewHelpers: '../app/admin/core/viewHelpers',
		cssComponents: '../../css/admin/components',
		velocity: 'velocity/velocity.min',
		'custom-scrollbar': 'malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min',
		'jquery-ui': 'jquery-ui-custom/jquery-ui.min',
		'skrollr': 'skrollr/skrollr.min',
		'SVG': 'svgjs/dist/svg',

		// Social
		fb: '../app/user/src/networks/fb-sdk',
		vk: '../app/user/src/networks/vk-sdk',
		ok: '../app/user/src/networks/ok-sdk'
	},
	map: {
		'*': {
			'css': 'require-css/css'
		}
	},
	shim: {
		'jquery': {
			exports: '$'
		},
		'underscore': {
			exports: '_'
		},
		'funcunit': {
			exports: 'F'
		},
		'canjs': {
			deps: [
				'jquery',
				'can/route/pushstate',
				'can/map/define',
				'can/map/delegate',
				'can/map/sort',
				'can/list/promise',
				'can/construct/super'
			]
		},
		velocity: {
			deps: [
				'jquery'
			]
		},
		'fb': {
			exports: 'FB'
		},
		'vk': {
			exports: 'VK'
		},
		'ok': {
			exports: 'OK'
		}

	},
	waitSeconds: 0,
	name: "core/core",
	// mainConfigFile: "public/js/app/user/core/core.js",
	out: "dist/bundle.min.js",
	optimize: 'none',
	stubModules: ['text', 'stache']
}