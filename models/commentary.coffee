mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId
Mixed = mongoose.Schema.Types.Mixed

SchemaFields =
	title:
		type: String
		trim: true
	content:
		type: String
		trim: true
	user:
		type: ObjectId
		ref: 'User'
	article:
		type: ObjectId
		ref: 'Article'
	date:
		type: Number
		default: Date.now

options =
	collection: 'commentaries'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Commentary', Schema
