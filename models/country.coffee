mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId
Mixed = mongoose.Schema.Types.Mixed

SchemaFields =
  lang: [
    languageId:
      type: ObjectId
      ref: 'Language'
    title:
      type: String
      trim: true
  ]
  mapId:
    type: String
    trim: true
  pointerTop:
    type: String
    trim: true
  pointerLeft:
    type: String
    trim: true
  color:
    type: String
    trim: true
    default: '#ffffff'
  colorOn:
    type: String
    trim: true
    default: '#ffffff'

options =
  collection: 'countries'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Country', Schema
