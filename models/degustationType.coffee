mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId

SchemaFields =
  lang: [
    languageId:
      type: ObjectId
      ref: 'Language'
    name:
      type: String
      trim: true
    snack:
      type: String
      trim: true
  ]
  price:
    type: Number

options =
  collection: 'degustationTypes'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'DegustationType', Schema
