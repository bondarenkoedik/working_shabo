mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId
Mixed = mongoose.Schema.Types.Mixed

SchemaFields =
	lang: [
		languageId:
			type: ObjectId
			ref: 'Language'
		title:
			type: String
			trim: true
		content:
			type: String
			trim: true
	]

options =
	collection: 'offers'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Offer', Schema
