mongoose = require 'mongoose'

ObjectId = mongoose.Schema.Types.ObjectId

SchemaFields =
  lang: [
    languageId:
      type: ObjectId
      ref: 'Language'
    name:
      type: String
      trim: true
  ]
  degustationType:
    type: ObjectId
    ref: 'DegustationType'
  position:
    type: Number
  img:
    type: String
    default: ''

options =
  collection: 'degustations'

Schema = new mongoose.Schema SchemaFields, options

module.exports =  mongoose.model 'Degustation', Schema
